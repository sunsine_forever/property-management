<?php

    use App\Models\Building;
    use Faker\Generator as Faker;

    /*
    |--------------------------------------------------------------------------
    | Model Factories
    |--------------------------------------------------------------------------
    |
    | This directory should contain each of the model factory definitions for
    | your application. Factories provide a convenient way to generate new
    | model instances for testing / seeding your application's database.
    |
    */

    $factory->define(Building::class, function (Faker $faker) {
        return [

            'project_name' => $faker->name,
            'brand_color' => $faker->name,
                'project_initial' => $faker->name ,
                'project_avatar' =>  $faker->image('public/storage/images',640,480, null, false) ,
                'horizontal_logo' => $faker->image('public/storage/images',640,480, null, false)
        ];
    });
