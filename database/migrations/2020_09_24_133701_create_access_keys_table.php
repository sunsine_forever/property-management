
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateAccessKeysTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("access_keys", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('number')->nullable();



						// ----------------------------------------------------
						// -- SELECT [access_keys]--
						// ----------------------------------------------------
						// $query = DB::table("access_keys")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("access_keys");
            }
        }
    