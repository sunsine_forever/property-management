
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateParkingListsTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("parking_lists", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('building_id')->unsigned();
						$table->integer('resident_id')->nullable()->unsigned();
						$table->integer('unit_id')->nullable()->unsigned();
						$table->integer('parking_id')->nullable()->unsigned();
						$table->foreign("building_id")->references("id")->on("building");
						$table->foreign("resident_id")->references("id")->on("resident");
						$table->foreign("unit_id")->references("id")->on("units");
						$table->foreign("parking_id")->references("id")->on("parkings");
                        $table->timestamps(0);


						// ----------------------------------------------------
						// -- SELECT [parking_lists]--
						// ----------------------------------------------------
						// $query = DB::table("parking_lists")
						// ->leftJoin("building","building.id", "=", "parking_lists.building_id")
						// ->leftJoin("resident","resident.id", "=", "parking_lists.resident_id")
						// ->leftJoin("units","units.id", "=", "parking_lists.unit_id")
						// ->leftJoin("parkings","parkings.id", "=", "parking_lists.parking_id")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("parking_lists");
            }
        }
    