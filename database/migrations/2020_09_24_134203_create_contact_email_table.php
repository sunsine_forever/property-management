
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateContactEmailTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("contact_email", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('contact_id')->nullable()->unsigned();
						$table->string('email')->nullable();
						$table->foreign("contact_id")->references("id")->on("contact");



						// ----------------------------------------------------
						// -- SELECT [contact_email]--
						// ----------------------------------------------------
						// $query = DB::table("contact_email")
						// ->leftJoin("contact","contact.id", "=", "contact_email.contact_id")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("contact_email");
            }
        }
    