
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateTaskTypeTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("task_type", function (Blueprint $table) {

						$table->increments('id');
						$table->string('name')->nullable();



						// ----------------------------------------------------
						// -- SELECT [task_type]--
						// ----------------------------------------------------
						// $query = DB::table("task_type")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("task_type");
            }
        }
    