
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateRemotesTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("remotes", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('number')->nullable();



						// ----------------------------------------------------
						// -- SELECT [remotes]--
						// ----------------------------------------------------
						// $query = DB::table("remotes")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("remotes");
            }
        }
    