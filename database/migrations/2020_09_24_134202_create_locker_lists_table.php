
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateLockerListsTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("locker_lists", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('building_id')->unsigned();
						$table->integer('unit_id')->nullable()->unsigned();
						$table->integer('resident_id')->nullable()->unsigned();
						$table->integer('locker_id')->nullable()->unsigned();
						$table->foreign("building_id")->references("id")->on("building");
						$table->foreign("unit_id")->references("id")->on("units");
						$table->foreign("resident_id")->references("id")->on("resident");
						$table->foreign("locker_id")->references("id")->on("lockers");
                        $table->timestamps(0);


						// ----------------------------------------------------
						// -- SELECT [locker_lists]--
						// ----------------------------------------------------
						// $query = DB::table("locker_lists")
						// ->leftJoin("building","building.id", "=", "locker_lists.building_id")
						// ->leftJoin("units","units.id", "=", "locker_lists.unit_id")
						// ->leftJoin("resident","resident.id", "=", "locker_lists.resident_id")
						// ->leftJoin("lockers","lockers.id", "=", "locker_lists.locker_id")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("locker_lists");
            }
        }
    