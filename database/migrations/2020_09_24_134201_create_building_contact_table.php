
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateBuildingContactTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("building_contact", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('building_id')->unsigned();
						$table->string('first_name',100)->nullable();
						$table->string('last_name',100)->nullable();
						$table->string('email',255)->nullable();
						$table->string('phone_number',255)->nullable();
						$table->integer('phone_ext')->nullable();
						$table->string('website',255)->nullable();
						$table->tinyInteger('is_private',1)->default(0);
						$table->foreign("building_id")->references("id")->on("building");



						// ----------------------------------------------------
						// -- SELECT [building_contact]--
						// ----------------------------------------------------
						// $query = DB::table("building_contact")
						// ->leftJoin("building","building.id", "=", "building_contact.building_id")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("building_contact");
            }
        }
    