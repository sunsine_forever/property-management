
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateRolesTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("roles", function (Blueprint $table) {

						$table->increments('id');
						$table->string('title')->nullable();
						$table->text('description')->nullable();
						$table->string('status',20)->nullable();



						// ----------------------------------------------------
						// -- SELECT [roles]--
						// ----------------------------------------------------
						// $query = DB::table("roles")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("roles");
            }
        }
    