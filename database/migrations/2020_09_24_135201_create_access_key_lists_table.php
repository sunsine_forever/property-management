
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateAccessKeyListsTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("access_key_lists", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('building_id')->unsigned();
						$table->integer('resident_id')->nullable()->unsigned();
						$table->integer('unit_id')->nullable()->unsigned();
						$table->integer('accesskey_id')->nullable()->unsigned();
						$table->foreign("building_id")->references("id")->on("building");
						$table->foreign("resident_id")->references("id")->on("resident");
						$table->foreign("unit_id")->references("id")->on("units");
						$table->foreign("accesskey_id")->references("id")->on("access_keys");
						$table->timestamps(0);

						// ----------------------------------------------------
						// -- SELECT [access_key_lists]--
						// ----------------------------------------------------
						// $query = DB::table("access_key_lists")
						// ->leftJoin("building","building.id", "=", "access_key_lists.building_id")
						// ->leftJoin("resident","resident.id", "=", "access_key_lists.resident_id")
						// ->leftJoin("units","units.id", "=", "access_key_lists.unit_id")
						// ->leftJoin("access_keys","access_keys.id", "=", "access_key_lists.accesskey_id")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("access_key_lists");
            }
        }
    