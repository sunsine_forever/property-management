
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateBuildingTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("building", function (Blueprint $table) {

						$table->increments('id');
						$table->string('project_name',255);
						$table->string('brand_color',10)->nullable();
						$table->string('project_initial',10)->nullable();
						$table->string('project_avatar',255)->nullable();
						$table->string('horizontal_logo',255)->nullable();



						// ----------------------------------------------------
						// -- SELECT [building]--
						// ----------------------------------------------------
						// $query = DB::table("building")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("building");
            }
        }
    