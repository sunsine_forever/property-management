
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateLockersTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("lockers", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('number')->nullable();
						$table->integer('cadastre_number')->nullable();



						// ----------------------------------------------------
						// -- SELECT [lockers]--
						// ----------------------------------------------------
						// $query = DB::table("lockers")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("lockers");
            }
        }
    