
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateParkingsTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("parkings", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('number')->nullable();
						$table->integer('cadastre_number')->nullable();
						$table->integer('proportion_share')->nullable();



						// ----------------------------------------------------
						// -- SELECT [parkings]--
						// ----------------------------------------------------
						// $query = DB::table("parkings")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("parkings");
            }
        }
    