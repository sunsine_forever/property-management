
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateContactPhoneTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("contact_phone", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('contact_id')->nullable()->unsigned();
						$table->string('phone',20)->nullable();
						$table->foreign("contact_id")->references("id")->on("contact");



						// ----------------------------------------------------
						// -- SELECT [contact_phone]--
						// ----------------------------------------------------
						// $query = DB::table("contact_phone")
						// ->leftJoin("contact","contact.id", "=", "contact_phone.contact_id")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("contact_phone");
            }
        }
    