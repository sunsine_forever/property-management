
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateContactDocumentsTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("contact_documents", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('contact_id')->nullable()->unsigned();
						$table->integer('document_id')->nullable()->unsigned();
						$table->foreign("contact_id")->references("id")->on("contact");
						$table->foreign("document_id")->references("id")->on("document");



						// ----------------------------------------------------
						// -- SELECT [contact_documents]--
						// ----------------------------------------------------
						// $query = DB::table("contact_documents")
						// ->leftJoin("contact","contact.id", "=", "contact_documents.contact_id")
						// ->leftJoin("document","document.id", "=", "contact_documents.document_id")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("contact_documents");
            }
        }
    