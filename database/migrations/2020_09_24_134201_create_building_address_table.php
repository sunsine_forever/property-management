
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateBuildingAddressTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("building_address", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('building_id')->unsigned();
						$table->string('address_line1',255)->nullable();
						$table->string('address_line2',255)->nullable();
						$table->string('city',255)->nullable();
						$table->string('postal_code',255)->nullable();
						$table->string('horizontal_logo',255)->nullable();
						$table->string('country',255)->nullable();
						$table->string('province',255)->nullable();
						$table->string('country_code',10)->nullable();
						$table->string('province_code',10)->nullable();
						$table->foreign("building_id")->references("id")->on("building");



						// ----------------------------------------------------
						// -- SELECT [building_address]--
						// ----------------------------------------------------
						// $query = DB::table("building_address")
						// ->leftJoin("building","building.id", "=", "building_address.building_id")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("building_address");
            }
        }
    