
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateUnitsTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("units", function (Blueprint $table) {

						$table->increments('id');
						$table->string('floor')->nullable();
						$table->string('type')->nullable();
						$table->integer('square_feet')->nullable();
						$table->integer('square_feet_balcony')->nullable();
						$table->integer('percent_square_footage')->nullable();
						$table->integer('rooms')->nullable();
						$table->text('note')->nullable();
						$table->integer('plan')->nullable();
						$table->integer('condo_fees')->nullable();
						$table->integer('intercom_code')->nullable();
						$table->integer('buzz_code')->nullable();
						$table->string('wifi_password')->nullable();
						$table->tinyInteger('has_alarm',1)->default(0);
						$table->integer('rent_amount')->nullable();
						$table->dateTime('rent_start')->nullable();
						$table->dateTime('rent_end')->nullable();
						$table->integer('lease')->nullable();
						$table->text('annexes')->nullable();
						$table->text('reference')->nullable();



						// ----------------------------------------------------
						// -- SELECT [units]--
						// ----------------------------------------------------
						// $query = DB::table("units")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("units");
            }
        }
    