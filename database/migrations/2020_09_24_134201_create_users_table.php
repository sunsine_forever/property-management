
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateUsersTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("users", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('role_id')->nullable()->unsigned();
						$table->string('name')->nullable();
						$table->string('email')->nullable();
						$table->string('password')->nullable();
						$table->string('title')->nullable();
						$table->string('phone')->nullable();
						$table->string('image')->nullable();
						$table->tinyInteger('is_admin',1)->default(0);
						$table->tinyInteger('is_active',1)->default(0);
						$table->foreign("role_id")->references("id")->on("roles");



						// ----------------------------------------------------
						// -- SELECT [users]--
						// ----------------------------------------------------
						// $query = DB::table("users")
						// ->leftJoin("roles","roles.id", "=", "users.role_id")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("users");
            }
        }
    