
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateSettingTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("setting", function (Blueprint $table) {

						$table->increments('id');
						$table->string('setting_key')->nullable();
						$table->string('setting_value')->nullable();
						$table->timestamps();



						// ----------------------------------------------------
						// -- SELECT [setting]--
						// ----------------------------------------------------
						// $query = DB::table("setting")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("setting");
            }
        }
    