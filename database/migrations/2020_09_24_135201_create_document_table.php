
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateDocumentTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("document", function (Blueprint $table) {

						$table->increments('id');
						$table->dateTime('expiration_date')->nullable();
						$table->dateTime('publish_date')->nullable();
						$table->integer('type')->nullable()->unsigned();
						$table->string('status')->nullable();
						$table->string('file')->nullable();
						$table->string('name')->nullable();
						$table->integer('createdby_id')->nullable()->unsigned();
						$table->integer('modifiedby_id')->nullable()->unsigned();
						$table->integer('assigneduser_id')->nullable()->unsigned();
						$table->foreign("type")->references("id")->on("document_type");
						$table->foreign("createdby_id")->references("id")->on("users");
						$table->foreign("modifiedby_id")->references("id")->on("users");
						$table->foreign("assigneduser_id")->references("id")->on("users");



						// ----------------------------------------------------
						// -- SELECT [document]--
						// ----------------------------------------------------
						// $query = DB::table("document")
						// ->leftJoin("document_type","document_type.id", "=", "document.type")
						// ->leftJoin("users","users.id", "=", "document.createdby_id")
						// ->leftJoin("users","users.id", "=", "document.modifiedby_id")
						// ->leftJoin("users","users.id", "=", "document.assigneduser_id")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("document");
            }
        }
    