
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateRemoteLogsTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("remote_logs", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('building_id')->unsigned();
						$table->integer('resident_id')->nullable()->unsigned();
						$table->integer('unit_id')->nullable()->unsigned();
						$table->integer('remote_id')->nullable()->unsigned();
						$table->foreign("building_id")->references("id")->on("building");
						$table->foreign("resident_id")->references("id")->on("resident");
						$table->foreign("unit_id")->references("id")->on("units");
						$table->foreign("remote_id")->references("id")->on("remotes");

                        $table->timestamps(0);

						// ----------------------------------------------------
						// -- SELECT [remote_logs]--
						// ----------------------------------------------------
						// $query = DB::table("remote_logs")
						// ->leftJoin("building","building.id", "=", "remote_logs.building_id")
						// ->leftJoin("resident","resident.id", "=", "remote_logs.resident_id")
						// ->leftJoin("units","units.id", "=", "remote_logs.unit_id")
						// ->leftJoin("remotes","remotes.id", "=", "remote_logs.remote_id")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("remote_logs");
            }
        }
    