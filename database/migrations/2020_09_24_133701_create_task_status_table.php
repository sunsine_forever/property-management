
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateTaskStatusTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("task_status", function (Blueprint $table) {

						$table->increments('id');
						$table->string('name')->nullable();



						// ----------------------------------------------------
						// -- SELECT [task_status]--
						// ----------------------------------------------------
						// $query = DB::table("task_status")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("task_status");
            }
        }
    