<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //Create resident list
        DB::raw("
create or replace view resident_list as 

select 
g.name as group_name, re.number as remote_number, p.number as parking_number, l.number as locker_number, ak.number as access_key_number,

r.*

from resident as r inner join units as u on u.id=r.livingunit_id
left join groups as g on g.id=r.group_id
left join remotes as re on re.id=r.remote_id
left join lockers as l on l.id=r.locker_id
left join access_keys as ak on ak.id=r.access_key_id
left join parkings as p on p.id=r.parking_id");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view');
    }
}
