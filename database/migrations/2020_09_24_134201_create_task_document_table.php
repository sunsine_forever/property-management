
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateTaskDocumentTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("task_document", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('task_id')->nullable()->unsigned();
						$table->integer('document_id')->nullable()->unsigned();
						$table->foreign("task_id")->references("id")->on("task");
						$table->foreign("document_id")->references("id")->on("document");



						// ----------------------------------------------------
						// -- SELECT [task_document]--
						// ----------------------------------------------------
						// $query = DB::table("task_document")
						// ->leftJoin("task","task.id", "=", "task_document.task_id")
						// ->leftJoin("document","document.id", "=", "task_document.document_id")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("task_document");
            }
        }
    