
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateTicketStatusTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("ticket_status", function (Blueprint $table) {

						$table->increments('id');
						$table->string('name')->nullable();



						// ----------------------------------------------------
						// -- SELECT [ticket_status]--
						// ----------------------------------------------------
						// $query = DB::table("ticket_status")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("ticket_status");
            }
        }
    