
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateGroupLogsTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("group_logs", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('building_id')->unsigned();
						$table->integer('resident_id')->nullable()->unsigned();
						$table->integer('unit_id')->nullable()->unsigned();
						$table->integer('group_id')->nullable()->unsigned();
						$table->string('status')->nullable();
						$table->foreign("building_id")->references("id")->on("building");
						$table->foreign("resident_id")->references("id")->on("resident");
						$table->foreign("unit_id")->references("id")->on("units");
						$table->foreign("group_id")->references("id")->on("groups");
                        $table->timestamps(0);


						// ----------------------------------------------------
						// -- SELECT [group_logs]--
						// ----------------------------------------------------
						// $query = DB::table("group_logs")
						// ->leftJoin("building","building.id", "=", "group_logs.building_id")
						// ->leftJoin("resident","resident.id", "=", "group_logs.resident_id")
						// ->leftJoin("units","units.id", "=", "group_logs.unit_id")
						// ->leftJoin("groups","groups.id", "=", "group_logs.group_id")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("group_logs");
            }
        }
    