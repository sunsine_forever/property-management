
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateTicketDocumentTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("ticket_document", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('ticket_id')->nullable()->unsigned();
						$table->integer('document_id')->nullable()->unsigned();
						$table->foreign("ticket_id")->references("id")->on("ticket");
						$table->foreign("document_id")->references("id")->on("document");



						// ----------------------------------------------------
						// -- SELECT [ticket_document]--
						// ----------------------------------------------------
						// $query = DB::table("ticket_document")
						// ->leftJoin("ticket","ticket.id", "=", "ticket_document.ticket_id")
						// ->leftJoin("document","document.id", "=", "ticket_document.document_id")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("ticket_document");
            }
        }
    