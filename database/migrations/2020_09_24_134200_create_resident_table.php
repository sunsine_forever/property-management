
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateResidentTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("resident", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('livingunit_id')->nullable()->unsigned();
						$table->string('title',20)->nullable();
						$table->string('email',255)->nullable();
						$table->string('second_email',255)->nullable();
						$table->integer('email_notification')->nullable();
						$table->integer('sms_notification')->nullable();
						$table->tinyInteger('is_tenant',1)->default(0);
						$table->tinyInteger('status',1)->default(0);
						$table->string('mobile_number',20)->nullable();
						$table->string('home_number',20)->nullable();
						$table->string('office_number',20)->nullable();
						$table->string('office_number_ext',10)->nullable();
						$table->string('first_name')->nullable();
						$table->string('last_name')->nullable();
						$table->integer('board_member_id')->nullable();
						$table->integer('cmuser_id')->nullable()->unsigned();
						$table->integer('user_id')->nullable()->unsigned();
						//Start new fields
                        $table->integer('group_id')->nullable()->unsigned();
                        $table->integer('locker_id')->nullable()->unsigned();
                        $table->integer('access_key_id')->nullable()->unsigned();
                        $table->integer('remote_id')->nullable()->unsigned();
                        $table->integer('parking_id')->nullable()->unsigned();
                        $table->integer('building_id')->nullable()->unsigned();
                        //End new field
						$table->text('note')->nullable();
						$table->string('emergency_contact')->nullable();
						$table->foreign("livingunit_id")->references("id")->on("units");
						$table->foreign("user_id")->references("id")->on("users");

						//Start new constraints
                        $table->foreign("group_id")->references("id")->on("groups");
                        $table->foreign("locker_id")->references("id")->on("lockers");
                        $table->foreign("access_key_id")->references("id")->on("access_keys");
                        $table->foreign("remote_id")->references("id")->on("remotes");
                        $table->foreign("parking_id")->references("id")->on("parkings");
                        $table->foreign("building_id")->references("id")->on("building");
                        //End new constraints


						// ----------------------------------------------------
						// -- SELECT [resident]--
						// ----------------------------------------------------
						// $query = DB::table("resident")
						// ->leftJoin("units","units.id", "=", "resident.livingunit_id")
						// ->leftJoin("users","users.id", "=", "resident.user_id")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("resident");
            }
        }
    