
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateTicketTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("ticket", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('contact_id')->nullable()->unsigned();
						$table->integer('priority')->nullable();
						$table->integer('status_id')->nullable()->unsigned();
						$table->integer('type_id')->nullable()->unsigned();
						$table->dateTime('start_date')->nullable();
						$table->dateTime('end_date')->nullable();
						$table->dateTime('complete_date')->nullable();
						$table->string('contact_type')->nullable();
						$table->text('description')->nullable();
						$table->string('name')->nullable();
						$table->integer('createdby_id')->nullable()->unsigned();
						$table->integer('modifiedby_id')->nullable()->unsigned();
						$table->integer('assigneduser_id')->nullable()->unsigned();
						$table->foreign("contact_id")->references("id")->on("contact");
						$table->foreign("status_id")->references("id")->on("ticket_status");
						$table->foreign("type_id")->references("id")->on("ticket_type");
						$table->foreign("createdby_id")->references("id")->on("users");
						$table->foreign("modifiedby_id")->references("id")->on("users");
						$table->foreign("assigneduser_id")->references("id")->on("users");



						// ----------------------------------------------------
						// -- SELECT [ticket]--
						// ----------------------------------------------------
						// $query = DB::table("ticket")
						// ->leftJoin("contact","contact.id", "=", "ticket.contact_id")
						// ->leftJoin("ticket_status","ticket_status.id", "=", "ticket.status_id")
						// ->leftJoin("ticket_type","ticket_type.id", "=", "ticket.type_id")
						// ->leftJoin("users","users.id", "=", "ticket.createdby_id")
						// ->leftJoin("users","users.id", "=", "ticket.modifiedby_id")
						// ->leftJoin("users","users.id", "=", "ticket.assigneduser_id")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("ticket");
            }
        }
    