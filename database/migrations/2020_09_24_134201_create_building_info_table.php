
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateBuildingInfoTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("building_info", function (Blueprint $table) {

						$table->increments('id');
						$table->integer('building_id')->unsigned();
						$table->integer('number_unit')->nullable();
						$table->text('add_info')->nullable();
						$table->tinyInteger('is_condo',1)->default(0);
						$table->tinyInteger('is_rental',1)->default(0);
						$table->tinyInteger('is_construction',1)->default(0);
						$table->integer('cmbuilding_id')->nullable()->unsigned();
						$table->foreign("building_id")->references("id")->on("building");



						// ----------------------------------------------------
						// -- SELECT [building_info]--
						// ----------------------------------------------------
						// $query = DB::table("building_info")
						// ->leftJoin("building","building.id", "=", "building_info.building_id")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("building_info");
            }
        }
    