
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateContactStatusTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("contact_status", function (Blueprint $table) {

						$table->increments('id');
						$table->string('name')->nullable();



						// ----------------------------------------------------
						// -- SELECT [contact_status]--
						// ----------------------------------------------------
						// $query = DB::table("contact_status")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("contact_status");
            }
        }
    