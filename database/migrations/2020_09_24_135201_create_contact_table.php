
    <?php
        use Illuminate\Support\Facades\Schema;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Database\Migrations\Migration;
        
        class CreateContactTable extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                Schema::create("contact", function (Blueprint $table) {

						$table->increments('id');
						$table->string('title',20)->nullable();
						$table->string('first_name')->nullable();
						$table->string('last_name')->nullable();
						$table->integer('status')->nullable()->unsigned();
						$table->string('referral_source')->nullable();
						$table->string('industry')->nullable();
						$table->string('project_type')->nullable();
						$table->text('project_description')->nullable();
						$table->text('description')->nullable();
						$table->string('budget')->nullable();
						$table->string('website')->nullable();
						$table->string('street')->nullable();
						$table->string('city')->nullable();
						$table->string('state')->nullable();
						$table->string('country')->nullable();
						$table->string('postal_code',10)->nullable();
						$table->integer('createdby_id')->nullable()->unsigned();
						$table->integer('modifiedby_id')->nullable()->unsigned();
						$table->integer('assigneduser_id')->nullable()->unsigned();
						$table->foreign("status")->references("id")->on("contact_status");
						$table->foreign("createdby_id")->references("id")->on("users");
						$table->foreign("modifiedby_id")->references("id")->on("users");
						$table->foreign("assigneduser_id")->references("id")->on("users");



						// ----------------------------------------------------
						// -- SELECT [contact]--
						// ----------------------------------------------------
						// $query = DB::table("contact")
						// ->leftJoin("contact_status","contact_status.id", "=", "contact.status")
						// ->leftJoin("users","users.id", "=", "contact.createdby_id")
						// ->leftJoin("users","users.id", "=", "contact.modifiedby_id")
						// ->leftJoin("users","users.id", "=", "contact.assigneduser_id")
						// ->get();
						// dd($query); //For checking



                });
            }

            /**
             * Reverse the migrations.
             *
             * @return void
             */
            public function down()
            {
                Schema::dropIfExists("contact");
            }
        }
    