<?php

    use App\Models\Building;
    use App\Models\BuildingAddress;
    use App\Models\BuildingContact;
    use App\Models\BuildingInfo;
    use Illuminate\Database\Seeder;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Str;

class BuildingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();
        for ($i=0; $i< 5; $i++){

            $buildingId = Building::insertGetId([
                'project_name' => $faker->name,
                'brand_color' => $faker->hexColor,
                'project_initial' => Str::random(5) ,
                'project_avatar' =>  $faker->image('public/storage/images',640,480, null, false) ,
                'horizontal_logo' => $faker->image('public/storage/images',640,480, null, false)
            ]);

            BuildingAddress::insert([
                'building_id' =>  $buildingId,
                'address_line1' => $faker->address,
                'address_line2' => $faker->address,
                'city' =>  $faker->city,
                'postal_code' => $faker->postcode ,
                'horizontal_logo'  => $faker->image('public/storage/images',640,480, null, false),
                'country' => $faker->country ,
                'province' => $faker->city ,
                'country_code' => $faker->countryCode ,
                'province_code' => $faker->countryCode
            ]);

            BuildingContact::insert([
                'building_id' => $buildingId ,
                'first_name' => $faker->firstName ,
                'last_name' =>  $faker->lastName,
                'email' => $faker->email ,
                'phone_number' => $faker->phoneNumber ,
                'phone_ext' => $faker->numberBetween(1,100),
                'website' =>  'http://' . $faker->company. '.com',
                'is_private' => $faker->boolean
            ]);

            BuildingInfo::insert([
                'building_id' => $buildingId,
                'number_unit' =>$faker->numberBetween(5,20) ,
                'add_info' => $faker->text(),
                'is_condo' => $faker->boolean ,
                'is_rental' => $faker->boolean,
                'is_construction' => $faker->boolean,
                'cmbuilding_id' => $faker->numberBetween(1,20)
            ]);
        }

    }
}
