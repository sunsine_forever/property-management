<?php

namespace App\Providers;

use App\Libraries\Utils;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        $this->loadBuilding();

    }

    private function loadBuilding()
    {

        view()->composer('layouts.sidebar', function ($view)
        {
            $buildings = Utils::buildingLeftMenus();
            $view->with(['buildings' => $buildings ]);

        });
    }

}
