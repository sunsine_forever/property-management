<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Parking extends Model
{
    protected $table = 'parkings';
    public $timestamps = false;
    protected $fillable = ['number','cadastre_number', 'proportion_share'];
}
