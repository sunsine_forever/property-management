<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuildingInfo extends Model
{
    protected $table = "building_info";
    public $timestamps = false;
    protected $fillable = ['building_id', 'number_unit', 'add_info', 'is_condo', 'is_rental', 'is_construction', 'cmbuilding_id'];
    // start of building
    public function building()
    {
        return $this->belongsTo('App\Models\Building', 'building_id');
    }// end of building
}
