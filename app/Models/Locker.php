<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Locker extends Model
{
    protected $table = 'lockers';
    public $timestamps = false;
    protected $fillable = ['number','cadastre_number'];
}
