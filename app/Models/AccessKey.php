<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccessKey extends Model
{
    protected $table = 'access_keys';
    public $timestamps = false;
    protected $fillable = ['number'];
}
