<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Remote extends Model
{
    protected $table = 'remotes';
    public $timestamps = false;
    protected $fillable = ['number'];
}
