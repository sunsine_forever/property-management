<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resident extends Model
{
    protected $table = 'resident';
    public $timestamps = false;
    protected $fillable = ['livingunit_id', 'second_email','email','sms_notification' , 'is_tenant',
        'mobile_number', 'home_number', 'office_number', 'office_number_ext',
        'first_name', 'last_name', 'board_member_id', 'cmuser_id', 'user_id', 'note', 'emergency_contact',
        'status', 'group_id', 'remote_id', 'parking_id', 'locker_id', 'access_key_id',
    ];


    // start of building
    public function building ()
    {
        return $this->belongsTo('App\Models\Building', 'building_id');
    }// end of building



    // start of unit
    public function unit ()
    {
        return $this->belongsTo('App\Models\Unit', 'livingunit_id');
    }// end of unit

}
