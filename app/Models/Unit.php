<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table = 'units';
    public $timestamps = false;
    protected $fillable = ['floor', 'rooms','note','type' , 'square_feet_balcony',
        'square_feet', 'plan', 'condo_fees', 'intercom_code',
        'wifi_password', 'has_alarm', 'buzz_code', 'rent_amount', 'rent_start', 'rent_end', 'lease',
        'annexes', 'reference', 'builiding_id'
    ];
    
    
    // start of building
    public function building ()
    {
        return $this->belongsTo('App\Models\Building', 'building_id');
    }// end of building


}
