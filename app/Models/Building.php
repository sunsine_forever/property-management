<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    protected $table = "building";
    public $timestamps = true;
    protected $fillable = ['project_name', 'brand_color', 'project_initial', 'project_avatar', 'horizontal_logo'];
    
    // start of address
    public function address()
    {
        return $this->hasOne('App\Models\BuildingAddress');
    }// end of address


    // start of contact
    public function contact()
    {
    return $this->hasOne('App\Models\BuildingContact');
    }// end of contact
    
    
    // start of info
    public function info()
    {
        return $this->hasOne('App\Models\BuildingInfo');
    
    }// end of info
}
