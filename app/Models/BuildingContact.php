<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuildingContact extends Model
{
    protected $table = "building_contact";
    public $timestamps = false;
    protected $fillable = ['building_id', 'first_name', 'last_name', 'email', 'phone_number', 'phone_ext', 'website', 'is_private'];

    // start of building
    public function building()
    {
        return $this->belongsTo('App\Models\Building', 'building_id');
    }// end of building
}
