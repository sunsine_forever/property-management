<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuildingAddress extends Model
{
    protected $table = "building_address";
    public $timestamps = false;

    protected $fillable = ['building_id', 'address_line1', 'address_line2', 'city', 'postal_code', 'horizontal_logo', 'country', 'province', 'country_code', 'province_code'];
    // start of building
    public function building()
    {
        return $this->belongsTo('App\Models\Building', 'building_id');
    }// end of building

}
