<?php

namespace App\Http\Controllers;

use App\Models\Building;
use Illuminate\Http\Request;
use DB;
use App\Models\Unit ;


class AjaxController extends Controller
{
    
    // start of loadBuilding
    public function loadBuilding(Request $request)
    {

        $by = 'row_num';

        if ($request->order[0]['column'] == 1)
        {
            $by = 'row_num';

        }elseif ($request->order[0]['column'] == 2)
        {
            $by = 'row_num';

        } elseif ($request->order[0]['column'] == 3)
        {
            $by = 'row_num';
        }


        if ($request->order[0]['dir'] == 'asc')
        {
            $order = 'asc';
        } else
        {
            $order = 'asc';
        }

        DB::select('SET @row_number = 0;');

        $projects = Building::join('building_address as ba', 'ba.building_id', '=','building.id')
                                -> join('building_contact as bc', 'bc.building_id', '=','building.id')
                                -> join('building_info as bi', 'bi.building_id', '=','building.id')
//                                ->select('id as row_id','project_name', 'brand_color', 'project_initial', 'project_avatar', '',DB::raw('(@row_number:=@row_number + 1) AS row_num '));
                                ->select('ba.*','bi.*','bc.*','building.*',DB::raw('(@row_number:=@row_number + 1) AS row_num '));

        if($request->project_name){
            $projects = $projects->where('project_name', $request->project_name);
        }

        if ($request->brand_color)
        {
            $projects = $projects->where('brand_color', $request->brand_color);
        }

        $projects = $projects->orderBy($by, $order);
        $obQ = clone $projects;


        if(isset($request->length)&&($request->length<0)){
            $dt = $projects->get()->toArray();

        }else{
            $dt = $projects->offset($request->start)
                ->limit($request->length)->get()->toArray();
        }

        $dtArr = $dt;
        $dataArr = $dt;


        $DT["draw"] = $request->draw;
        $DT["recordsTotal"] = $obQ->count();
        $DT["recordsFiltered"] = $obQ->count();

        $DT['data'] = array_values(array_values($dataArr));

        return response()->json($DT);

    
    }// end of loadBuilding
    
    
    // start of loadUnits
    public function loadUnits(Request $request)
    {
        $by = 'row_num';

        if ($request->order[0]['column'] == 1)
        {
            $by = 'row_num';

        }elseif ($request->order[0]['column'] == 2)
        {
            $by = 'row_num';

        } elseif ($request->order[0]['column'] == 3)
        {
            $by = 'row_num';
        }


        if ($request->order[0]['dir'] == 'asc')
        {
            $order = 'asc';
        } else
        {
            $order = 'asc';
        }

        DB::select('SET @row_number = 0;');

        $units = Unit::when($request->buildingId, function ($query, $cond){
                return $query->where('building_id', $cond);
        })
        ->select('units.*', DB::raw('(@row_number:=@row_number + 1) AS row_num '),'id as row_id');

        if($request->project_name){
            $units = $units->where('project_name', $request->project_name);
        }

        if ($request->brand_color)
        {
            $units = $units->where('brand_color', $request->brand_color);
        }


        $units = $units->orderBy($by, $order);
        $obQ = clone $units;


        if(isset($request->length)&&($request->length<0)){
            $dt = $units->get()->toArray();

        }else{
            $dt = $units->offset($request->start)
                ->limit($request->length)->get()->toArray();
        }

        $dtArr = $dt;
        $dataArr = $dt;

        $DT["draw"] = $request->draw;
        $DT["recordsTotal"] = $obQ->count();
        $DT["recordsFiltered"] = $obQ->count();

        $DT['data'] = array_values(array_values($dataArr));

        return response()->json($DT);


    }// end of loadUnits


    // start of loadUnits
    public function loadResidents(Request $request)
    {
        $by = 'row_num';

        if ($request->order[0]['column'] == 1)
        {
            $by = 'row_num';

        }elseif ($request->order[0]['column'] == 2)
        {
            $by = 'row_num';

        } elseif ($request->order[0]['column'] == 3)
        {
            $by = 'row_num';
        }


        if ($request->order[0]['dir'] == 'asc')
        {
            $order = 'asc';
        } else
        {
            $order = 'asc';
        }


        DB::select('SET @row_number = 0;');
        $units = DB::table('resident_list')->when($request->buildingId, function ($query, $cond){
            return $query->where('building_id', $cond);
        })->select('resident_list.*', DB::raw('(@row_number:=@row_number + 1) AS row_num '),'id as row_id');

        if($request->project_name){
            $units = $units->where('project_name', $request->project_name);
        }

        if ($request->brand_color)
        {
            $units = $units->where('brand_color', $request->brand_color);
        }


        $units = $units->orderBy($by, $order);
        $obQ = clone $units;


        if(isset($request->length)&&($request->length<0)){
            $dt = $units->get()->toArray();

        }else{
            $dt = $units->offset($request->start)
                ->limit($request->length)->get()->toArray();
        }

        $dtArr = $dt;
        $dataArr = $dt;

        $DT["draw"] = $request->draw;
        $DT["recordsTotal"] = $obQ->count();
        $DT["recordsFiltered"] = $obQ->count();

        $DT['data'] = array_values(array_values($dataArr));

        return response()->json($DT);


    }// end of loadUnits
    
    
    // start of loadTickets
    public function loadTickets()
    {

    
    }// end of loadTickets

}
