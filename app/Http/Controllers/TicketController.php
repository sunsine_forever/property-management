<?php

namespace App\Http\Controllers;

use App\Libraries\Utils;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

class TicketController extends Controller
{
    public $view_root_path = '.tickets.';

    // start of showTickets
    public function showTickets()
    {
        $data['pageTitle'] = $data['title'] = 'Tickets';
        try {
            if(is_null(session('ticket_search_access_token'))){
                Utils::tokenGenerator(env('ZOHO_TICKET_SEARCH_REFRESH_TOKEN'),'Desk.search.READ,Desk.tickets.READ','ticket_search_access_token');
            }
           $response =  Http::withToken(session('ticket_access_token'))->get(env('ZOHO_BASE_URL'). "tickets/search",[
            'limit'=> 10
            ]);

           if($response->status()!==200){
               Utils::tokenGenerator(env('ZOHO_TICKET_SEARCH_REFRESH_TOKEN'),'Desk.search.READ,Desk.tickets.READ','ticket_search_access_token');
           }
           $responseBody = $response->body();
//           dd($responseBody);
           $rows = json_decode($responseBody);
            $data['rows']  = $rows->data;
            return view($this->view_root_path.'index', $data);

        }catch (\HttpException $e){
            dd($e->getStatusCode());
        }
    
    }// end of showTickets

    // start of deleteTicket
    public function ticketMoveToTrash($ticketId)
    {
        try {
            if(is_null(session('access_token'))){
                Utils::tokenGenerator(env('ZOHO_TICKET_ALL_REFRESH_TOKEN'),'Desk.tickets.ALL','access_token');
            }

            $response =  Http::withToken(session('access_token'))->post(env('ZOHO_BASE_URL') . "tickets/moveToTrash",[
                'ticketIds'=> [$ticketId]
            ]);
            if($response->status()!==200){
                Utils::tokenGenerator(env('ZOHO_TICKET_ALL_REFRESH_TOKEN'),'Desk.tickets.ALL','access_token');
            }

            return response()->json($response->status());


        }catch (\HttpException $e){
            dd($e->getStatusCode());
        }
    
    }// end of deleteTicket

    // start of showComments
    public function showComments($ticketId)
    {
        $data['pageTitle'] = $data['title'] = 'Ticket';

        try{
            if(is_null(session('access_token'))){
                Utils::tokenGenerator(env('ZOHO_TICKET_ALL_REFRESH_TOKEN'),'Desk.tickets.ALL','access_token');
            }

//            //Start ticket
            $ticketResponse =  Http::withToken(session('access_token'))->get(env('ZOHO_BASE_URL') . "tickets/" . $ticketId,[
                'include'=> 'departments,assignee'
            ]);
            if($ticketResponse->status()!==200){
                Utils::tokenGenerator(env('ZOHO_TICKET_ALL_REFRESH_TOKEN'),'Desk.tickets.ALL','access_token');
            }
            $ticketResponseBody = $ticketResponse->body();
            $ticket = json_decode($ticketResponseBody);
            $data['ticket']  = $ticket;
//            //End ticket


            //Start comment
            $response =  Http::withToken(session('access_token'))->get(env('ZOHO_BASE_URL') . "tickets/" . $ticketId . "/comments",[
                'include'=> 'mentions'
            ]);

            $responseBody = $response->body();
            $rows = json_decode($responseBody);
            $data['comments']  = $rows->data;
            $data['ticketId'] = $ticketId;
            return view($this->view_root_path.'comments', $data);

        }catch (\HttpException $e){
            dd($e->getStatusCode());
        }
    
    }// end of showComments

    // start of addComment
    public function addComment(Request $request, $ticketId)
    {
        try{
            if(is_null(session('access_token'))){
                Utils::tokenGenerator(env('ZOHO_TICKET_ALL_REFRESH_TOKEN'),'Desk.tickets.ALL','access_token');
            }

            $content = $request->commentContent;
            $fullUrl = env('ZOHO_BASE_URL') . "tickets/" . $ticketId . "/comments";
//
            $ticketResponse =  Http::withToken(session('access_token'))->post($fullUrl,[
                "isPublic"=> "true",
                "contentType"=> "plainText",
                "content" => $content
            ]);
//            dd($ticketResponse);


            if($ticketResponse->status()!==200){
                Utils::tokenGenerator(env('ZOHO_TICKET_ALL_REFRESH_TOKEN'),'Desk.tickets.ALL','access_token');
            }

            return redirect("tickets/" . $ticketId ."/comments");
//            $ticketResponseBody = $ticketResponse->body();
//            $ticket = json_decode($ticketResponseBody);
//            dd($ticket);
//            $data['ticket']  = $ticket;
//            //End ticket
        }catch (\Exception $e){
dd($e);
        }
    
    }// end of addComment

    // start of closeTicket
    public function closeTicket($ticketId)
    {
        try {
        if(is_null(session('access_token'))){
            Utils::tokenGenerator(env('ZOHO_TICKET_ALL_REFRESH_TOKEN'),'Desk.tickets.ALL','access_token');
        }

        $response =  Http::withToken(session('access_token'))->post(env('ZOHO_BASE_URL') . "closeTickets",[
        'ids' =>  [$ticketId]
            ]);

        if($response->status()!==200){
            Utils::tokenGenerator(env('ZOHO_TICKET_ALL_REFRESH_TOKEN'),'Desk.tickets.ALL','access_token');
        }
        return response()->json($response->status());


    }catch (\HttpException $e){

        dd($e->getStatusCode());
    }
    
    }// end of closeTicket

}
