<?php

namespace App\Http\Controllers;

use App\Http\Requests\UnitRequest;
use App\Libraries\Utils;
use App\Models\Unit;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    public $view_root_path = '.units.';

    // start of showUnits
    public function showUnit($buildingId)
    {
        $data['pageTitle'] = $data['title'] = 'Units';
        $data['buildingId'] = $buildingId;
        return view($this->view_root_path.'index', $data);
    }// end of showUnits


    // start of showAddForm
    public function showAddForm($buildingId)
    {
        $data['pageTitle'] =  $data['title'] = 'Unit';
        $data['subBtnText'] = "Add";
        $data['buildingId'] = $buildingId;
        return view($this->view_root_path.'form', $data);

    }// end of showAddForm


   // start of saveUnitForm
   public function saveUnitForm(UnitRequest $request,$buildingId)
   {
       $validated = $request->validated();
       $requests = $request->all();
        $requests['building_id'] = $buildingId;
       //Save data on Unit
       Utils::saveFormData($requests, 'units');

       $flashMessage = Utils::getFlashMessage('success', 'Added new unit successfully');
       \Session::flash('flash_message', $flashMessage); //<--FLASH MESSAGE
       return redirect('building/units/'. $buildingId);
   }// end of saveUnitForm
    
    
    // start of showEditForm
    public function showEditForm($buildingId, $unitId)
    {
        $data['pageTitle'] =  $data['title'] = 'Unit';
        $data['subBtnText'] = "Update";
        $data['buildingId'] = $buildingId;
        $data['unit'] = Unit::find($unitId);
        return view($this->view_root_path.'form', $data);
    }// end of showEditForm
    
    
    // start of updateUnitFormData
    public function updateUnitFormData(UnitRequest $request, $buildingId, $unitId)
    {
        $validated = $request->validated();
        $requests = $request->all();
        //Update unit
        Utils::updateFormData($requests, 'units','id', $unitId);

        $flashMessage = Utils::getFlashMessage('success', 'Unit has been successfully updated.');
        \Session::flash('flash_message', $flashMessage); //<--FLASH MESSAGE
        return redirect('building/units/'.$buildingId);
    }// end of updateUnitFormData
    

    
    

}
