<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUpdateBuilding;
use App\Libraries\Utils;
use App\Models\Building;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class BuildingController extends Controller
{
    public $view_root_path = '.building.';

    // start of index
    public function index()
    {
        $data['pageTitle'] =  $data['title'] = 'Building';

        return view($this->view_root_path.'index', $data);
    }// end of index
    
    
    // start of addNew
    public function showAddForm(Request $request)
    {

        $data['pageTitle'] =  $data['title'] = 'Building ';
        $data['subBtnText'] = "Add";

        $data['country'] =  Utils::countrySelector("INDIA", "country_name", "country_name", "form-control");

        return view($this->view_root_path.'form', $data);
    
    }// end of addNew
    
    
    // start of store
    public function saveBuilding(StoreUpdateBuilding $request)
    {
        $validated = $request->validated();

        $requests = $request->all();

        $country  = explode("|" ,$requests["country_name"] );
        $requests['country'] = $country[0];
        $requests['country_code'] = $country[1];

        //Save data on Building
      $buildingId =   Utils::saveFormData($requests, 'building');
      $requests['building_id'] = $buildingId;

      // Save building address
        Utils::saveFormData($requests, 'building_address');
        //Save building contact
        Utils::saveFormData($requests, 'building_contact');
        //Save building info
        Utils::saveFormData($requests, 'building_info');

        $flashMessage = Utils::getFlashMessage('success', 'Added new building ' . $request-> project_name . ' successfully');
        \Session::flash('flash_message', $flashMessage); //<--FLASH MESSAGE

        return redirect('manage/building/index');

    }// end of store
    
   
   // start of buildingEdit
   public function showEditForm($id)
   {
       $data['pageTitle'] =  $data['title'] = 'Building';
       $data['subBtnText'] = "Update";

       $data['building'] = Building::find($id);

       $data['country'] =  Utils::countrySelector( $data["building"]->address->country, "country_name", "country_name", "form-control");

       return view($this->view_root_path.'form', $data);


   }// end of buildingEdit 
    
    
    // start of update
    public function updateBuilding(StoreUpdateBuilding $request , $id)
    {
        $validated = $request->validated();
        $requests = $request->all();
        $country  = explode("|" ,$requests["country_name"] );
        $requests['country'] = $country[0];
        $requests['country_code'] = $country[1];

        //Update building
        $buildingId =   Utils::updateFormData($requests, 'building','id', $id);
        // Update building address
        Utils::updateFormData($requests, 'building_address', 'building_id', $id);

        //Update building contact
        Utils::updateFormData($requests, 'building_contact', 'building_id', $id);

        //Update building Info
        Utils::updateFormData($requests, 'building_info', 'building_id', $id);

        $flashMessage = Utils::getFlashMessage('success', $request->project_name . ' has been successfully updated.');
        \Session::flash('flash_message', $flashMessage); //<--FLASH MESSAGE
        return redirect('manage/building/index');





    }// end of update
    
}
