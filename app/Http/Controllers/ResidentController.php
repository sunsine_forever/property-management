<?php

namespace App\Http\Controllers;

use App\Http\Requests\ResidentRequest;
use App\Libraries\Utils;
use App\Models\AccessKey;
use App\Models\Group;
use App\Models\Locker;
use App\Models\Parking;
use App\Models\Remote;
use App\Models\Resident;
use App\Models\Unit;
use Illuminate\Http\Request;

class ResidentController extends Controller
{
    public $view_root_path = '.residents.';
    protected $titles = [
        'Mr.',
        'Ms',
        'Mrs.',
        'Miss'
    ];
    // start of showResident
    public function showResident($buildingId)
    {
        $data['pageTitle'] = $data['title'] = 'Residents';
        $data['buildingId'] = $buildingId;
        return view($this->view_root_path.'index', $data);
    }// end of showResident


    // start of showAddForm
    public function showAddForm($buildingId)
    {
        $data['pageTitle'] =  $data['title'] = 'Resident';
        $data['subBtnText'] = "Add";
        $data['buildingId'] = $buildingId;
        $data['titles'] = $this->titles;
        $data['units'] = Unit::where('building_id', $buildingId)->select('id')->get()->collect();
        $data['groups'] = Group::pluck('name', 'id');
        $data['remotes'] = Remote::pluck('number', 'id');
        $data['parkings'] = Parking::pluck('number', 'id');
        $data['lockers'] = Locker::pluck('number', 'id');
        $data['access_keys'] = AccessKey::pluck('number', 'id');

        return view($this->view_root_path.'form', $data);

    }// end of showAddForm


    // start of saveResidentForm
    public function saveResidentForm(ResidentRequest $request,$buildingId)
    {
        $validated = $request->validated();
        $requests = $request->all();
        $requests['building_id'] = $buildingId;
        //Save data on Resident
        Utils::saveFormData($requests, 'resident');

        //TODO if sms notification is true then trigger sms event
        //TODO if email notification is true then trigger email event

        $flashMessage = Utils::getFlashMessage('success', 'Added new resident successfully');
        \Session::flash('flash_message', $flashMessage); //<--FLASH MESSAGE
        return redirect('building/residents/'. $buildingId);
    }// end of saveResidentForm


    // start of showEditForm
    public function showEditForm($buildingId, $residentId)
    {
        $data['pageTitle'] =  $data['title'] = 'Resident';
        $data['subBtnText'] = "Update";
        $data['buildingId'] = $buildingId;
        $data['resident'] = Resident::find($residentId);
        $data['titles'] = $this->titles;
        $data['units'] = Unit::where('building_id', $buildingId)->select('id')->get()->collect();
        $data['groups'] = Group::pluck('name', 'id');
        $data['remotes'] = Remote::pluck('number', 'id');
        $data['parkings'] = Parking::pluck('number', 'id');
        $data['lockers'] = Locker::pluck('number', 'id');
        $data['access_keys'] = AccessKey::pluck('number', 'id');

        return view($this->view_root_path.'form', $data);
    }// end of showEditForm

    // start of updateResidentFormData
    public function updateResidentFormData(ResidentRequest $request, $buildingId, $residentId)
    {
        $validated = $request->validated();
        $requests = $request->all();
        //Update resident
        Utils::updateFormData($requests, 'resident','id', $residentId);

        $flashMessage = Utils::getFlashMessage('success', 'Resident has been successfully updated.');
        \Session::flash('flash_message', $flashMessage); //<--FLASH MESSAGE
        return redirect('building/residents/'.$buildingId);
    }// end of updateResidentFormData





}
