<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ManageController extends Controller
{
    public $view_root_path = '.manage.';

    // start of showGroup
    public function showGroup()
    {
        $data['pageTitle'] =  $data['title'] = 'Blood';
        return view($this->view_root_path.'group_index', $data);
    }// end of showGroup

    // start of showAccessKeys
    public function showAccessKeys()
    {
        $data['pageTitle'] =  $data['title'] = 'Blood';

        return view($this->view_root_path.'accesskey_index', $data);
    }// end of showAccessKeys

    // start of showRemotes
    public function showRemotes()
    {
        $data['pageTitle'] =  $data['title'] = 'Blood';

        return view($this->view_root_path.'remote_index', $data);
    }// end of showRemotes


    // start of showParking
    public function showParking()
    {
        $data['pageTitle'] =  $data['title'] = 'Blood';

        return view($this->view_root_path.'parking_index', $data);
    }// end of showParking

    // start of showLocker
    public function showLocker()
    {
        $data['pageTitle'] =  $data['title'] = 'Blood';

        return view($this->view_root_path.'locker_index', $data);
    }// end of showLocker

}
