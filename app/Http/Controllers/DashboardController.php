<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public $view_root_path = '.dashboard.';

    // start of index
    public function index()
    {
//        Route::view('dashboard/dashboard2', 'dashboard.dashboardv2')->name('dashboard_version_2');
        $data['title'] = $data['pageTitle'] = 'Dashboard';
        return view($this->view_root_path.'dashboardv1', $data);
    }// end of index
}
