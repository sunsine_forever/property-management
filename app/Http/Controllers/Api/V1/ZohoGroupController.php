<?php
    namespace App\Http\Controllers\Api\V1;

    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use App\Libraries\Utils;
    use Illuminate\Support\Facades\Http;

class ZohoGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fullUrl  = env('ZOHO_MAIL_BASE_URL') . 'organization/' . env('ZOHO_ORGANIZATION_ID') .  '/groups';

        try {
            if(is_null(session('group_access_token'))){
                Utils::tokenGenerator(env('ZOHO_MAIL_GROUP_REFRESH_TOKEN'),'ZohoMail.organization.groups.CREATE,ZohoMail.organization.groups.DELETE,ZohoMail.organization.groups.UPDATE,ZohoMail.organization.groups.READ','group_access_token');
            }
            $zohoResponse =  Http::withToken(session('group_access_token'))->get($fullUrl);
            if($zohoResponse->status()!==200){
                Utils::tokenGenerator(env('ZOHO_MAIL_GROUP_REFRESH_TOKEN'),'ZohoMail.organization.groups.CREATE,ZohoMail.organization.groups.DELETE,ZohoMail.organization.groups.UPDATE,ZohoMail.organization.groups.READ','group_access_token');
            }
            $responseBody = $zohoResponse->body();
            $rows = json_decode($responseBody);
            $data['data']  = $rows->data;
            return response()->json($data);
        }catch (\HttpException $e){
            dd($e->getStatusCode());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
