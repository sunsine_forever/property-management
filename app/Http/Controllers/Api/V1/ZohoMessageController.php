<?php
    namespace App\Http\Controllers\Api\V1;

    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use App\Libraries\Utils;
    use Illuminate\Support\Facades\Http;

class ZohoMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fullUrl  = env('ZOHO_MAIL_BASE_URL') . 'accounts/' . env('ZOHO_ACCOUNT_ID') .  '/messages/view';

        try {
            if(is_null(session('message_access_token'))){
                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');
            }
            $zohoResponse =  Http::withToken(session('message_access_token'))->get($fullUrl);
            if($zohoResponse->status()!==200){
                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');
            }
            $responseBody = $zohoResponse->body();
            $rows = json_decode($responseBody);
            $data['data']  = $rows->data;
            return response()->json($data);
        }catch (\HttpException $e){
            dd($e->getStatusCode());
        }
    }
    
    
    // start of messageByFolder
    public function messageByFolder($folderId)
    {

          $fullUrl  = env('ZOHO_MAIL_BASE_URL') . 'accounts/' . env('ZOHO_ACCOUNT_ID') .  '/messages/view';

        try {
            if(is_null(session('message_access_token'))){
                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');
            }
            $zohoResponse =  Http::withToken(session('message_access_token'))->get($fullUrl,[
                'folderId' =>  $folderId
            ]);
            if($zohoResponse->status()!==200){
                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');
            }

            $responseBody = $zohoResponse->body();
            $rows = json_decode($responseBody);
            $data['data']  = $rows->data;
            return response()->json($data);
        }catch (\HttpException $e){
            dd($e->getStatusCode());
        }
    
    }// end of messageByFolder

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $fullUrl  = env('ZOHO_MAIL_BASE_URL') . 'accounts/' . env('ZOHO_ACCOUNT_ID') .  '/messages';

        try {
            if(is_null(session('message_access_token'))){
                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');

            }

            $requestParams = [
                'fromAddress' =>  env('ZOHO_MAIL_FROM_ADDRESS'),
                'toAddress' =>  trim(html_entity_decode($request->to)),
                'subject'=> $request->subject,
                'content'=> $request->body,
//                'askReceipt'=>'yes'
            ];

            if($request->has('fileName') && $request->fileName !=null){
                foreach ($request->fileName as $file)
                {
                    $fullUrlAttachment = env('ZOHO_MAIL_BASE_URL') . 'accounts/' . env('ZOHO_ACCOUNT_ID') . '/messages/attachments';
                    $finalUrl = $fullUrlAttachment . '?fileName=' . $file->getClientOriginalName();
//                $finalUrl = $fullUrlAttachment.'?fileName='.$request->file('fileName')->getClientOriginalName();

//                    $contents = file_get_contents($request->file('fileName')->getRealPath());
                    $contents = file_get_contents($file->getRealPath());
                    $zohoAttachmentResponse = Http::withToken(session('message_access_token'))->attach(
                        $file->getClientOriginalName(), $contents,
//                        $request->file('fileName')->getClientOriginalName(), $contents,
                        $file->getClientOriginalName()
//                        $request->file('fileName')->getClientOriginalName()
                    )->withHeaders([
                        'Content-Type' => 'application/octet-stream',
                    ])->post($finalUrl);

                    $attachmentResponseBody = $zohoAttachmentResponse->body();
                    $attachments = json_decode($attachmentResponseBody);
                    $attachment = (array) $attachments->data;
                    $requestParams["attachments"][] = $attachment; // $attachments->data;
                }
            }

            $zohoResponse =  Http::withToken(session('message_access_token'))->post($fullUrl,$requestParams);

            if($zohoResponse->status()!==200){
                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');
            }
            $responseBody = $zohoResponse->body();
            $rows = json_decode($responseBody);
            $data['data']  = $rows->data;
            return response()->json($data);
        }catch (\HttpException $e){
            dd($e->getStatusCode());
        }


    }

    
    // start of pushForwardMail
    public function pushForwardMail(Request $request)
    {
//        dd($request->all());
        $fullUrl  = env('ZOHO_MAIL_BASE_URL') . 'accounts/' . env('ZOHO_ACCOUNT_ID') .  '/messages';

        try {
            if(is_null(session('message_access_token'))){
                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');

            }

            $requestParams = [
                'fromAddress' =>  env('ZOHO_MAIL_FROM_ADDRESS'),
                'toAddress' =>  trim(html_entity_decode($request->to)),
                'subject'=> $request->subject,
                'content'=> $request->body,
//                'askReceipt'=>'yes'
            ];

            if($request->has('fileName') && $request->fileName !=null){

                foreach ($request->fileName as $file)
                {

                    $fullUrlAttachment = env('ZOHO_MAIL_BASE_URL') . 'accounts/' . env('ZOHO_ACCOUNT_ID') . '/messages/attachments';
                    $finalUrl = $fullUrlAttachment . '?fileName=' . $file->getClientOriginalName();

                    $contents = file_get_contents($file->getRealPath());
                    $zohoAttachmentResponse = Http::withToken(session('message_access_token'))->attach(
                        $file->getClientOriginalName(), $contents,
                        $file->getClientOriginalName()
                    )->withHeaders([
                        'Content-Type' => 'application/octet-stream',
                    ])->post($finalUrl);

                    $attachmentResponseBody = $zohoAttachmentResponse->body();
                    $attachments = json_decode($attachmentResponseBody);

                    $attachment = (array) $attachments->data;
                    $requestParams["attachments"][] = $attachment; // $attachments->data;
                } //end foreach

            } //end if

            $zohoResponse =  Http::withToken(session('message_access_token'))->post($fullUrl,$requestParams);

            if($zohoResponse->status()!==200){
                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');
            }
            $responseBody = $zohoResponse->body();
            $rows = json_decode($responseBody);
            $data['data']  = $rows->data;
            return response()->json($data);
        }catch (\HttpException $e){
            dd($e->getStatusCode());
        }

    }// end of pushForwardMail


    public function showSingleMessage($folderId , $msgId)
    {

        $fullUrl  = env('ZOHO_MAIL_BASE_URL') . 'accounts/' . env('ZOHO_ACCOUNT_ID') .  '/folders/'.$folderId.'/messages/'.$msgId.'/content';

        try {
            if(is_null(session('message_access_token'))){
                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');
            }
            $zohoResponse =  Http::withToken(session('message_access_token'))->get($fullUrl);
            if($zohoResponse->status()!==200){
                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');
            }
            $responseBody = $zohoResponse->body();
            $rows = json_decode($responseBody);
            $data['data']  = $rows->data;
            //start Attachment
            $fullAttachmentUrl  = env('ZOHO_MAIL_BASE_URL') . 'accounts/' . env('ZOHO_ACCOUNT_ID') .  '/folders/'.$folderId.'/messages/'.$msgId.'/attachmentinfo';

//            if(is_null(session('message_access_token'))){
//                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');
//            }
            $zohoAttachmentResponse =  Http::withToken(session('message_access_token'))->get($fullAttachmentUrl);
            if($zohoAttachmentResponse->status()!==200){
                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');
            }
            $zohoAttachmentResponseBody = $zohoAttachmentResponse->body();
            $attachment = json_decode($zohoAttachmentResponseBody);
            $data['attachments']  = $attachment->data;
            //end Attachment

            return response()->json($data);
        }catch (\HttpException $e){
            dd($e->getStatusCode());
        }

    }
    
    
    // start of pushReplyMail
    public function pushReplyMail(Request $request, $msgId)
    {

        $fullUrl  = env('ZOHO_MAIL_BASE_URL') . 'accounts/' . env('ZOHO_ACCOUNT_ID') .  '/messages/' . $request->messageId;
        try {
            if(is_null(session('message_access_token'))){
                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');

            }

            $requestParams = [
                'fromAddress' =>  trim(html_entity_decode($request->fromAddress)),
                'toAddress' => trim(html_entity_decode($request->toAddress)),
                'subject'=> $request->subject,
                'content'=> $request->body,
                'askReceipt'=>'yes',
                'action' => 'reply'
            ];

            if($request->has('fileName') && $request->fileName !=null){
                foreach ($request->fileName as $file)
                {
                    $fullUrlAttachment = env('ZOHO_MAIL_BASE_URL') . 'accounts/' . env('ZOHO_ACCOUNT_ID') . '/messages/attachments';
                    $finalUrl = $fullUrlAttachment . '?fileName=' . $file->getClientOriginalName();

                    $contents = file_get_contents($file->getRealPath());
                    $zohoAttachmentResponse = Http::withToken(session('message_access_token'))->attach(
                        $file->getClientOriginalName(), $contents,
                        $file->getClientOriginalName()
                    )->withHeaders([
                        'Content-Type' => 'application/octet-stream',
                    ])->post($finalUrl);

                    $attachmentResponseBody = $zohoAttachmentResponse->body();
                    $attachments = json_decode($attachmentResponseBody);
                    $attachment = (array) $attachments->data;
                    $requestParams["attachments"][] = $attachment; // $attachments->data;
                }
            }

            $zohoResponse =  Http::withToken(session('message_access_token'))->post($fullUrl,$requestParams);
            if($zohoResponse->status()!==200){
                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');
            }
            $responseBody = $zohoResponse->body();
            $rows = json_decode($responseBody);
            $data['data']  = $rows->data;
            return response()->json($data);
        }catch (\HttpException $e){
            dd($e->getStatusCode());
        }
    
    }// end of pushReplyMail
    
    
    
    // start of deleteMessage
    public function deleteMessage(Request $request)
    {
        $folderId = $request->folderId;

        try {
            if(is_null(session('message_access_token'))){
                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');

            }
    foreach ($request->ids as $messageId)
    {
        $fullUrl = env('ZOHO_MAIL_BASE_URL') . 'accounts/' . env('ZOHO_ACCOUNT_ID') . '/folders/' . $folderId . '/messages/' . $messageId;
        $zohoResponse = Http::withToken(session('message_access_token'))->delete($fullUrl);
    }
            if($zohoResponse->status()!==200){
                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');
            }
            $responseBody = $zohoResponse->body();
            $rows = json_decode($responseBody);
            return response()->json($rows);
        }catch (\HttpException $e){
            dd($e->getStatusCode());
        }

    }// end of deleteMessage

    // start of showMessageSearchResult
    public function showMessageSearchResult(Request $request)
    {
        $searchKey = $request->searchKey;
        $searchRequest = "subject:" . $searchKey . "::summary:".$searchKey;
        $fullUrl  = env('ZOHO_MAIL_BASE_URL') . 'accounts/' . env('ZOHO_ACCOUNT_ID') .  '/messages/search';

        try {
            if(is_null(session('message_access_token'))){
                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');
            }
            $zohoResponse =  Http::withToken(session('message_access_token'))->get($fullUrl,[
                'searchKey' =>$searchRequest
            ]);
            if($zohoResponse->status()!==200){
                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');
            }
            $responseBody = $zohoResponse->body();
            $rows = json_decode($responseBody);
            $data['data']  = $rows->data;
            return response()->json($data);
        }catch (\HttpException $e){
            dd($e->getStatusCode());
        }

    
    }// end of showMessageSearchResult
    
    
    // start of setMessageAsImportant
    public function setMessageAsImportant(Request $request)
    {
        $msgId = $request->messageId;
        $fullUrl  = env('ZOHO_MAIL_BASE_URL') . 'accounts/' . env('ZOHO_ACCOUNT_ID') .  '/updatemessage';

        try {
            if(is_null(session('message_access_token'))){
                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');
            }
            $zohoResponse =  Http::withToken(session('message_access_token'))->put($fullUrl,[
                "mode" => "setFlag",
                "flagid"=> "important",
                "messageId"=> [$msgId]
            ]);
            if($zohoResponse->status()!==200){
                Utils::tokenGenerator(env('ZOHO_MESSAGE_REFRESH_TOKEN'),'ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.messages.UPDATE,ZohoMail.messages.READ','message_access_token');
            }
            $responseBody = $zohoResponse->body();
            $rows = json_decode($responseBody);
//            $data['data']  = $rows->data;
            return response()->json($rows);
        }catch (\HttpException $e){
            dd($e->getStatusCode());
        }
    
    }// end of setMessageAsImportant
}
