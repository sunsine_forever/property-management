<?php

    namespace App\Http\Controllers\Api\V1;
    use Illuminate\Support\Facades\Validator;
    use Illuminate\Http\Request;
    use App\Models\Group;
    use App\Http\Controllers\Controller;
    class GroupController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            $items = Group::select('name','id','description')->orderBy('id','DESC')->paginate(10);

            $response = [
                'pagination' => [
                    'total' => $items->total(),
                    'per_page' => $items->perPage(),
                    'current_page' => $items->currentPage(),
                    'last_page' => $items->lastPage(),
                    'from' => $items->firstItem(),
                    'to' => $items->lastItem()
                ],
                'data' => $items
            ];

            return response()->json($response, 200, [], JSON_NUMERIC_CHECK);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            //
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {

            $validator = Validator::make($request->all(), [
                'name' => 'required:unique:groups,name',
            ]);

            if ($validator->fails())
            {
                return response()->json($validator);
            }

            $isContain = Group::where('name',$request->name)->count();
            if($isContain<1){
                $response['status'] = 1;

                $create = Group::create($request->all());
            }else{
                $response['status'] = false;
            }

            return response()->json($response);
        }

        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }



        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $this->validate($request, [
                'name' => 'required',
            ]);

            $edit = Group::find($id)->update($request->all());

            return response()->json($edit);
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            Group::find($id)->delete();
            return response()->json(['done']);
        }
    }
