<?php

namespace App\Http\Controllers;

use App\Libraries\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class MessageController extends Controller
{
    public $view_root_path = '.message.';

    // start of showMessage
    public function showMessage()
    {
        $data['pageTitle'] = $data['title'] = 'Message';

//        try {
//            if(is_null(session('access_token'))){
//                Utils::tokenGenerator(env('ZOHO_TICKET_SEARCH_REFRESH_TOKEN'),'Desk.search.READ,Desk.tickets.READ');
//            }
//            $response =  Http::withToken(session('access_token'))->get(env('ZOHO_BASE_URL'). "tickets/search",[
//                'limit'=> 10
//            ]);
//
//            if($response->status()!==200){
//                Utils::tokenGenerator(env('ZOHO_TICKET_SEARCH_REFRESH_TOKEN'),'Desk.tickets.ALL');
//            }
//            $responseBody = $response->body();
//            $rows = json_decode($responseBody);
//            $data['rows']  = $rows->data;
//            return view($this->view_root_path.'index', $data);
//
//        }catch (\HttpException $e){
//            dd($e->getStatusCode());
//        }

        return view($this->view_root_path.'index', $data);
    }// end of showMessage

    
    // start of uploadForm
    public function uploadForm(Request $request)
    {
        $data['pageTitle'] = $data['title'] = 'Message';
        if ($request->isMethod('post')){
            dd($request->all());
        }
        return view($this->view_root_path.'test', $data);
    }// end of uploadForm

}
