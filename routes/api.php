<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

    Route::group(['prefix' => 'v1/app', 'namespace' => 'Api\V1', 'as' => 'api.'], function () {
        Route::resource('group', 'GroupController');
        Route::resource('locker', 'LockerController');
        Route::resource('accesskeys', 'AccessKeyController');
        Route::resource('parking', 'ParkingController');
        Route::resource('remote', 'RemoteController');

        //Folder
        Route::resource('folders','ZohoFolderController');
        Route::resource('groups','ZohoGroupController');

        Route::resource('messages','ZohoMessageController');\
        Route::get('messages/singlemessage/{folderId}/{msgId}','ZohoMessageController@showSingleMessage');


        Route::get('messagesbyfolder/{folderId}','ZohoMessageController@messageByFolder');
        Route::post('pushReplyMail/{msgId}','ZohoMessageController@pushReplyMail');
        Route::post('messagedelete', 'ZohoMessageController@deleteMessage');
        Route::get('searchresults', 'ZohoMessageController@showMessageSearchResult'); //Message search result
        Route::post('setmessageasimportant', 'ZohoMessageController@setMessageAsImportant');

        Route::post('pushForwardMail','ZohoMessageController@pushForwardMail');

    });

