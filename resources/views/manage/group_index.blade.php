@extends('layouts.master')

@section('page-css')

    <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection


@section('main-content')
    <div class="breadcrumb">
        <h1>Manage</h1>
        <ul>
            <li><a href="">Dashboard</a></li>
            <li>Groups</li>
        </ul>
    </div>

    <div class="separator-breadcrumb border-top"></div>

{{--<div style="float:right; clear: both">--}}


    <a href="{{url('manage/remotes')}}" class="btn btn-primary ripple m-1"> <i class="icon-list"></i> Remotes  </a>
    <a href="{{url('manage/parkings')}}" class="btn btn-primary ripple m-1"> <i class="icon-list"></i>  Parking  </a>
    <a href="{{url('manage/lockers')}}" class="btn btn-primary ripple m-1"> <i class="icon-list"></i> Lockers  </a>
    <a href="{{url('manage/accesskeys')}}" class="btn btn-primary ripple m-1"> <i class="icon-list"></i> Access Keys </a>

    <a href="#add-group" class="btn btn-info ripple m-1" data-toggle="modal" data-target="#add-group"> <i class="icon-plus"></i> Add Group</a>
{{--</div>--}}
    <div class="row mb-4">
        <!-- start col-md-12 -->
        <div class="col-md-12  mb-4">
            <div class="card text-left">

                <div class="card-body">
                    <h4 class="card-title mb-3">Groups</h4>

                    <div class="table-responsive">
                        <table id="user_table" class="table  text-center">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                      <tbody>
                      <tr v-for="(item, index) in items">
                          <th v-text="index + 1"></th>
                          <td v-text="item.name"></td>
                          <td v-text="item.description"></td>

                          <td>
                              <a class="btn btn-light ripple m-1" @click.prevent="editItem(item)" > <span class="label label-info"><i class="icon-note"></i> Edit</span></a>

                          </td>
                      </tr>
                      </tbody>
                    </table>
                    </div>

                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item" v-if="pagination.current_page > 1">
                                <a class="page-link" href="#" tabindex="-1" @click.prevent="changePage(pagination.current_page - 1)">Previous</a>
                            </li>
                            <li class="page-item" v-for="page in pagesNumber"
                                v-bind:class="[ page == isActived ? 'active' : '']">
                                <a class="page-link" href="#" @click.prevent="changePage(page)" v-html="page">1</a></li>

                            <li class="page-item" v-if="pagination.current_page < pagination.last_page">
                                <a class="page-link" href="#"  @click.prevent="changePage(pagination.current_page + 1)">Next</a>
                            </li>
                        </ul>
                    </nav>



                </div>

            </div>
        </div> <!-- end col-md-12 -->
    </div> <!-- end row -->

    <!-- Create Item Modal -->
    <div class="modal fade" id="add-group" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Add Group</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>

                </div>
                <div class="modal-body">

                    <form method="POST" enctype="multipart/form-data" v-on:submit.prevent="createItem">

                        <div class="form-group">
                            <label for="title" class="control-label">Group:</label>
                            <input type="text" name="name" class="form-control" v-model="newItem.name" />
                            <span v-if="formErrors['name']" class="error text-danger">@{{ formErrors['name'] }}</span>
                        </div>

                        <div class="form-group">
                            <label for="title" class="control-label">description:</label>
                            <input type="text" name="description" class="form-control" v-model="newItem.description" />
                            <span v-if="formErrorsUpdate['description']" class="error text-danger">@{{ formErrors['description'] }}</span>
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn btn-success ripple m-1"><i class="icon-plus"></i>  Add</button>
                        </div>

                    </form>


                </div>
            </div>
        </div>
    </div>

    <!-- edit-->

    <!-- Edit Item Modal -->
    <div class="modal fade" id="edit-group" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Group</h4>
                </div>
                <div class="modal-body">

                    <form method="POST" enctype="multipart/form-data" v-on:submit.prevent="updateItem(fillItem.id)">

                        <div class="form-group">
                            <label for="title" class="control-label">Name:</label>
                            <input type="text" name="name" class="form-control" v-model="fillItem.name" />
                            <span v-if="formErrorsUpdate['name']" class="error text-danger">@{{ formErrorsUpdate['name'] }}</span>
                        </div>

                        <div class="form-group">
                            <label for="title" class="control-label">description:</label>
                            <input type="text" name="description" class="form-control" v-model="fillItem.description" />
                            <span v-if="formErrorsUpdate['description']" class="error text-danger">@{{ formErrorsUpdate['description'] }}</span>
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn btn-success ripple m-1"> <i class="icon-note"></i> Update</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

    {{--</div>--}}



@endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    {{--<script src="{{asset('assets/js/datatables.script.js')}}"></script>--}}


    <script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>

    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.2.1/vue.js"></script>

    <script>



        Vue.prototype.$http = axios;

        var vue = new Vue({
            el: '#app',
            data: {
                items: [],
                pagination: {
                    total: 0,
                    per_page: 10,
                    from: 1,
                    to: 0,
                    current_page: 1
                },
                offset: 10,
                formErrors:{},
                formErrorsUpdate:{},
                newItem : {'name':'', 'description': ''},
                fillItem : {'name':'', 'description': ''},
                status:'',
                rowId:'',
                type:'',
            },
            computed: {
                isActived: function () {
                    return this.pagination.current_page;
                },
                pagesNumber: function () {
                    if (!this.pagination.to) {
                        return [];
                    }
                    var from = this.pagination.current_page - this.offset;
                    if (from < 1) {
                        from = 1;
                    }
                    var to = from + (this.offset * 2);
                    if (to >= this.pagination.last_page) {
                        to = this.pagination.last_page;
                    }
                    var pagesArray = [];
                    while (from <= to) {
                        pagesArray.push(from);
                        from++;
                    }
                    return pagesArray;
                }
            },
            mounted() {

                this.getVueItems(this.pagination.current_page);
            },

            methods: {

                getVueItems: function (page) {
                    var vm = this;
                    //get header data
                    this.$http.get("{{url('api/v1/app/group')}}", {params: {page: page}}).then(function (response) {
                        vm.$set(vm,'items', response.data.data.data);
                        vm.$set(vm,'pagination', response.data.pagination);
                    }).catch(function (error) {
                        console.log(error);
                    });
                },

                createItem: function () {
                    var vm = this;
                    //get header data
                    var input = this.newItem;
                    this.$http.post("{{url('api/v1/app/group')}}", input).then(function (response) {

                        vm.changePage(vm.pagination.current_page);
                        vm.newItem = {'name':'', 'description': ''};
                        $("#add-group").modal('hide');
                        if(response.status){
                            console.log(response);
                            toastr.success('Group added successfully.', 'Success Alert', {timeOut: 5000});
                        }else{
                            toastr.danger('Group already added.', 'Success Alert', {timeOut: 5000});
                        }

                    }).catch(function (error) {
                        console.log(error);
                    });
                },

                editItem: function(item){

                    this.fillItem.name = item.name;
                    this.fillItem.id = item.id;
                    this.fillItem.description = item.description;
                    $("#edit-group").modal('show');
                },

                updateItem: function(id){
                    var vm = this;
                    var input = this.fillItem;
                    this.$http.put("{{url('api/v1/app/group')}}/"+id,input).then(function(response) {
                        vm.changePage(vm.pagination.current_page);
                        this.fillItem = {'name':'','id':'', 'description': ''};
                        $("#edit-group").modal('hide');
                        toastr.success('Group updated successfully.', 'Success Alert', {timeOut: 5000});
                    }).catch(function (response){
                        this.formErrorsUpdate = response.data;
                    });
                },
                changePage: function (page) {
                    this.pagination.current_page = page;
                    this.getVueItems(page);
                }
            } // end of methods

        })

    </script>

@endsection
