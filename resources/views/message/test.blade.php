@extends('layouts.master')
@section('page-css')
    <link
            rel="stylesheet"
            href="https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css"
    />
    <style>
        body {
            padding: 20px;
        }

        .btn-file {
            position: relative;
            overflow: hidden;
        }

        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }
    </style>
@endsection


@section('main-content')
    @include('partials.flash-message')

    <div class="breadcrumb">
        <h1>{{$pageTitle}}</h1>

    </div>
    <div class="separator-breadcrumb border-top"></div>


    <div class="row">
        <div class="col-md-12 mb-3">

            {{--<div class="container">--}}
                {{--<!-- The file upload form used as target for the file upload widget -->--}}
                {{--<form id="fileupload" action="#" method="POST" enctype="multipart/form-data">--}}

                    <div class="row files" id="files1">
                        <span class="btn btn-success fileinput-button btn btn-default btn-file">
                    Browse  <input type="file" name="files1" multiple />
                </span>
                        <br />
                        <ul class="fileList"></ul>
                    </div>

                {{--</form>--}}
            {{--</div>--}}



        </div>
    </div>
@endsection

@section('page-js')

    <script>
        $.fn.fileUploader = function (filesToUpload, sectionIdentifier) {
            var fileIdCounter = 0;

            this.closest(".files").change(function (evt) {
                var output = [];

                for (var i = 0; i < evt.target.files.length; i++) {
                    fileIdCounter++;
                    var file = evt.target.files[i];
                    var fileId = sectionIdentifier + fileIdCounter;

                    filesToUpload.push({
                        id: fileId,
                        file: file
                    });

                    var removeLink = "<a class=\"removeFile \" href=\"#\" data-fileid=\"" + fileId + "\"><i class='i-Close-Window'></i></a>";

                    output.push("<li class='btn btn-outline-secondary m-1'><strong>", escape(file.name), "</strong> &nbsp; ", removeLink, "</li> ");
                };

                $(this).children(".fileList")
                    .append(output.join(""));

                //reset the input to null - nice little chrome bug!
                evt.target.value = null;
            });

            $(this).on("click", ".removeFile", function (e) {
                e.preventDefault();

                var fileId = $(this).parent().children("a").data("fileid");

                // loop through the files array and check if the name of that file matches FileName
                // and get the index of the match
                for (var i = 0; i < filesToUpload.length; ++i) {
                    if (filesToUpload[i].id === fileId)
                        filesToUpload.splice(i, 1);
                }

                $(this).parent().remove();
            });

            this.clear = function () {
                for (var i = 0; i < filesToUpload.length; ++i) {
                    if (filesToUpload[i].id.indexOf(sectionIdentifier) >= 0)
                        filesToUpload.splice(i, 1);
                }

                $(this).children(".fileList").empty();
            }

            return this;
        };

        (function () {
            var filesToUpload = [];

            var files1Uploader = $("#files1").fileUploader(filesToUpload, "files1");
        })()
    </script>

@endsection
