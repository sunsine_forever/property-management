@extends('layouts.master')
@section('page-css')
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/sweetalert2.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/toastr.css')}}">
{{--    <link rel="stylesheet" href="{{asset('assets/styles/vendor/dropzone.min.css')}}">--}}
    <link
            rel="stylesheet"
            href="https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css"
    />
    <link rel="stylesheet" href="{{asset('assets/js/vendor/jquery-file-upload/css/jquery.fileupload.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/js/vendor/jquery-file-upload/css/jquery.fileupload-ui.css')}}" />
    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <noscript
    ><link rel="stylesheet" href="{{asset('assets/js/vendor/jquery-file-upload/css/jquery.fileupload-noscript.css')}}"
        /></noscript>
    <noscript
    ><link rel="stylesheet" href="{{asset('assets/js/vendor/jquery-file-upload/css/jquery.fileupload-ui-noscript.css')}}"
        /></noscript>

<style>
    .avatar {
        font-family: Arial, Helvetica, sans-serif;
        /*width: 2.5rem;*/
        /*height: 2.5rem;*/
        height: 13% !important;
        width: 13% !important;
        border-radius: 50%;
        font-size: 0.8rem;
        color: #000;
        text-align: center;
        line-height: 2.4rem;
        margin: 0.5rem 9px;
        border: 3px solid #0c540c;
        font-weight: bold;
    }

    .mail-active {
        /*position: absolute;*/
        width: 0;
        /*height: 0;*/
        right: 0;
        bottom: 0;
        border-color: transparent transparent green;
        border-style: solid;
        border-width: 0 0 30px 30px;
        padding-left: 0px;
        margin-right: -19px;
        margin-bottom: -20px;
    }

    .btn-file {
        position: relative;
        overflow: hidden;
    }

    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

</style>
@endsection


@section('main-content')
    @include('partials.flash-message')

    <div class="breadcrumb">
        <h1>{{$pageTitle}}</h1>

    </div>
    <div class="separator-breadcrumb border-top"></div>



    <!-- MAIN SIDEBAR CONTAINER -->
    <div data-sidebar-container="main" class="inbox-main-sidebar-container">
        <div data-sidebar-content="main" class="inbox-main-content">
            <!-- SECONDARY SIDEBAR CONTAINER -->
            <div data-sidebar-container="secondary" class="inbox-secondary-sidebar-container box-shadow-1">
                <div data-sidebar-content="secondary">
                    <div class="inbox-secondary-sidebar-content position-relative" style="min-height: 500px">

                        <div class="card mb-4" v-if="view_compose == 1">
                        {{--<div class="card mb-4" >--}}
                            <div class="card-header" style="background-color: #4caf50; color: #FFF;font-size: 12px ">
                                <span style="text-align: left;font-size: 14px;"> New Message</span>

                            </div>
                            <div class="card-body">
                                <form method="POST" enctype="multipart/form-data" v-on:submit.prevent="sendEmail">
                                    {{--@endif--}}
                                    {{--@csrf--}}
                                    <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="to_address" class="col-sm-2 col-form-label">To: </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="to_address" placeholder="To address"   name="to_address" required v-model="email.to">
                                            <div class="invalid-feedback">
                                                Please provide a to address.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="cc_address" class="col-sm-2 col-form-label">Cc: </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="first_name" placeholder="CC"  name="cc_address" v-model="email.cc">
                                            <div class="invalid-feedback">
                                                Please provide a cc address.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label for="subject" class="col-sm-2 col-form-label">Subject: </label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="subject" placeholder="Enter a subject"  name="subject" v-model="email.subject">
                                                    <div class="invalid-feedback">
                                                        Please provide a mail subject.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-12 form-group mb-3">
                                            {{--<textarea  type="text" class="full-editor" placeholder=" Message ..." required  name="commentContent" style="width: 100%" v-model="email.body"> </textarea>--}}
                                            {{--<textarea  type="text" class="full-editor" placeholder=" Message ..." required  name="commentContent" style="width: 100%" v-model="email.body"> </textarea>--}}
                                            <vue-editor v-model="email.body" />

                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group row">
                                            <div class="col-sm-10">


                                                <div class="row files" id="files1">
                        <span class="btn btn-success fileinput-button btn btn-default btn-file" >
                            Select files

                             <input  type="file" id="file" ref="file" v-on:change="fileUpload()" multiple />
                </span>
                                                    <br />
                                                    <ul class="fileList" >
                                                        <li class='btn btn-outline-secondary m-1' v-for="(file,index) in output" @click="removeFile(file)"><strong v-html="file">   </strong> &nbsp;
                                                            <a class="removeFile " href="javascript:;"> <i class='i-Close-Window'></i></a>
                                                        </li>
                                                    </ul>
                                                </div>



                                            </div>
                                            {{--<label for="subject" class="col-sm-2 col-form-label"> Attachments</label>--}}

                                        </div>
                                        </div>

                                        <br>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12 mb-3" style="margin-top: 50px">
                                            <button class="btn btn-success" style="float: right;" >Soumettre</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                        <!--COMPOSE EMAIL -->

                        <div class="inbox-topbar box-shadow-1 perfect-scrollbar pl-3" data-suppress-scroll-y="true" v-if="view_compose == 2" style="background-color: #000; height: 40px">
                            <!-- <span class="d-sm-none">Test</span> -->
                            <a data-sidebar-toggle="main" class="link-icon d-md-none">
                                <i class="icon-regular i-Arrow-Turn-Left" style="color: green"></i>
                            </a>
                            <a data-sidebar-toggle="secondary" class="link-icon mr-3 d-md-none">
                                <i class="icon-regular mr-1 i-Left-3"></i>
                                Inbox
                            </a>

                            <div class="d-flex">
                                <a href="javascript:;" class="link-icon mr-3" @click="is_reply=1">
                                    {{--<i class="icon-regular i-Left"></i>--}}
                                    <img src="{{url('assets/images/reply.png')}}" alt="" width="20"/>
                                   &nbsp; &nbsp; <span style="color: white">Reply</span></a>
                                &nbsp;
                                <a href="javascript:;"  class="link-icon mr-3" @click="is_reply=2"> <img src="{{url('assets/images/transfer.png')}}" alt=""/> &nbsp; <span style="color: #fff;">Transfer</span></a>

                                <a href="javascript:;"  class="link-icon mr-3" @click="makeMessageImportant(message.messageId)">
                                    <img src="{{url('assets/images/etoile.png')}}" alt=""/> &nbsp;
                                    {{--<i class="icon-regular i-Left"></i>--}}
                                    <span style="color: #fff;">Important</span></a>

                            </div>
                        </div>
                        <!-- EMAIL DETAILS -->

                        <div class="inbox-details perfect-scrollbar" data-suppress-scroll-x="true" v-if="view_compose == 2">

                            <h4 class="mb-3" v-text="message.subject"></h4>

                            <div class="row no-gutters">
                                <span class="spinner-glow spinner-glow-primary mr-5" v-if="!message.sender"></span>
                                {{--<span v-text="message.length"></span>--}}
                                {{--<div class="mr-2" style="width: 20px">--}}
                                    {{--<div class="avatar" v-html="avatar(message.sender)">--}}
                                        {{--<img src="{{asset('/assets/images/faces/1.jpg')}}" alt="">--}}
                                        {{--<b v-html="avatar(message.sender)"></b>--}}
                                    {{--</div>--}}
                                    <img class="rounded-circle" src="{{asset('/assets/images/faces/1.jpg')}}" alt="" width="40">
                                {{--</div>--}}
                                <div class="col-xs-12">
                                    <p class="m-0" ><span v-text="message.sender"></span> (<span v-text="message.fromAddress"></span>)</p>
                                    {{--<p class="text-5 text-muted" v-text="receivedDate(message.receivedTime)"> </p>--}}
                                    <span class="text-5 text-muted"> Oct 03 , 2020 </span>
                                </div>
                            </div>
                            <hr style="margin-top: 1rem; margin-bottom: 1rem;">

                            <div v-if="!singleMessage.content">
                                <span class="spinner-glow spinner-glow-primary mr-5" ></span>
                            </div>
                            <div v-html="singleMessage.content" v-else>
                            </div>

                            <button class="btn btn-light m-1" type="button" v-for="attachment in attachments">
                                <img src="{{asset('/assets/images/attachment.png')}}" alt="" width="14">
                                <span v-text="attachment.attachmentName"></span>
                            </button>
                            <br>
                            {{--<a href="javascript:;" class="link-icon mr-3" @click="is_reply=1">--}}
                                {{--<i class="icon-regular i-Left"></i>--}}
                                {{--<img src="{{url('assets/images/reply.png')}}" alt="" width="20"/>--}}
                                {{--&nbsp; &nbsp; <span style="color: white">Reply</span></a>--}}

                            <button   class="btn btn-success m-1 btn-sm" @click="is_reply=2" style="float: right;"> <img src="{{url('assets/images/transfer.png')}}" alt=""/> &nbsp; <span style="color: #fff;">Transfer</span></button>
                            <button class="btn btn-success m-1 btn-sm" type="button" @click="is_reply=1" style="float: right">  <i class="icon-regular i-Left" style="font-size: 15px;"></i>  &nbsp; Reply &nbsp;</button>

                            <br>
                        </div>

                        <div class="card mb-4" v-if="is_reply == 1 && view_compose==2" style="width: 95%; float: right; z-index: 1000"> <!-- Reply mail -->
                            <div class="card-header" style="background-color: #4caf50; color: #FFF;font-size: 12px ">
                                Re :  <span style="text-align: left;font-size: 14px;" v-text="message.subject"> </span>

                            </div>
                            <div class="card-body">
                                <form method="POST" enctype="multipart/form-data" v-on:submit.prevent="pushReplyMail">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label for="to_address" class="col-sm-3 col-form-label">From: </label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="to_address" placeholder="From address"   name="from_address" required v-model="replyEmail.fromAddress">
                                                    <div class="invalid-feedback">
                                                        Please provide a to address.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label for="cc_address" class="col-sm-3 col-form-label">To: </label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="first_name" placeholder="to_address"  name="to_address" v-model="replyEmail.toAddress">
                                                    <div class="invalid-feedback">
                                                        Please provide a cc address.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label for="subject" class="col-sm-3 col-form-label">Subject: </label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="subject" placeholder="Enter a subject"  name="subject" v-model="replyEmail.subject">
                                                    <div class="invalid-feedback">
                                                        Please provide a mail subject.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-12 form-group mb-3">
                                            {{--<textarea  class="full-editor" placeholder=" Message ..." required  name="commentContent" style="width: 100%" v-model="replyEmail.body"> </textarea >--}}
                                            <vue-editor v-model="replyEmail.body" />

                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <div class="col-sm-10">
                                                    {{--<input  class="custom-control-input" type="file" id="file" ref="file" v-on:change="handleFileUpload()" multiple/>--}}
                                                    {{--<input d type="file" id="file" ref="file" v-on:change="handleFileUpload()" multiple/>--}}
                                                    {{--<input  type="file" id="file" ref="file" v-on:change="handleFileUpload()" />--}}
                                                    <div class="row files" id="files1">
                        <span class="btn btn-success fileinput-button btn btn-default btn-file" >
                            Select files

                             <input  type="file" id="file" ref="file" v-on:change="fileUpload()" multiple />
                </span>
                                                        <br />
                                                        <ul class="fileList" >
                                                            <li class='btn btn-outline-secondary m-1' v-for="(file,index) in output" @click="removeFile(file)"><strong v-html="file">   </strong> &nbsp;
                                                                <a class="removeFile " href="javascript:;"> <i class='i-Close-Window'></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                </div>
                                                {{--<label for="subject" class="col-sm-2 col-form-label"> Attachments</label>--}}

                                            </div>
                                        </div>


                                        <br>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12 mb-3" style="margin-top: 50px">
                                            <button class="btn btn-success" style="float: right;" >Reply</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card mb-4" v-if="is_reply == 2 && view_compose==2" style="width: 95%; float: right; z-index: 1000;"> <!-- Transfer mail -->
                            <div class="card-header" style="background-color: #4caf50; color: #FFF;font-size: 12px ">
                                Fwd :  <span style="text-align: left;font-size: 14px;" v-text="message.subject"> </span>

                            </div>
                            <div class="card-body">
                                <form method="POST" enctype="multipart/form-data" v-on:submit.prevent="pushForwardMail">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label for="to_address" class="col-sm-3 col-form-label">From: </label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="to_address" placeholder="From address"   name="from_address" required v-model="forwardEmail.fromAddress">
                                                    <div class="invalid-feedback">
                                                        Please provide a to address.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label for="cc_address" class="col-sm-3 col-form-label">To: </label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="first_name" placeholder="to_address"  name="to_address" v-model="forwardEmail.toAddress">
                                                    <div class="invalid-feedback">
                                                        Please provide a cc address.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label for="subject" class="col-sm-3 col-form-label">Subject: </label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="subject" placeholder="Enter a subject"  name="subject" v-model="forwardEmail.subject">
                                                    <div class="invalid-feedback">
                                                        Please provide a mail subject.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-12 form-group mb-3">
                                            <vue-editor v-model="forwardEmail.body" />

                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <div class="col-sm-10">
                                                    {{--<input  class="custom-control-input" type="file" id="file" ref="file" v-on:change="handleFileUpload()" multiple/>--}}
                                                    {{--<input d type="file" id="file" ref="file" v-on:change="handleFileUpload()" multiple/>--}}
                                                    {{--<input  type="file" id="file" ref="file" v-on:change="handleFileUpload()" />--}}
                                                    <div class="row files" id="files1">
                        <span class="btn btn-success fileinput-button btn btn-default btn-file" >
                            Select files

                             <input  type="file" id="file" ref="file" v-on:change="fileUpload()" multiple />
                </span>
                                                        <br />
                                                        <ul class="fileList" >
                                                            <li class='btn btn-outline-secondary m-1' v-for="(file,index) in output" @click="removeFile(file)"><strong v-html="file">   </strong> &nbsp;
                                                                <a class="removeFile " href="javascript:;"> <i class='i-Close-Window'></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                {{--<label for="subject" class="col-sm-2 col-form-label"> Attachments</label>--}}

                                            </div>
                                        </div>



                                        <br>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12 mb-3" style="margin-top: 50px">
                                            <button class="btn btn-success" style="float: right;" >Transfer</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Secondary Inbox sidebar -->
                <div data-sidebar="secondary" class="inbox-secondary-sidebar perfect-scrollbar">
                    <i class="sidebar-close i-Close" data-sidebar-toggle="secondary"></i>

                       <div class="mail-item " >
                           <div class="row">
                               <div class="col-md-2">
                                   <button type="button" class="btn btn-primary btn-icon rounded-circle m-1" style="width: 36px;
height: 36px;margin: 0; " @click="view_compose=1">
                               <span class="ul-btn__icon">
                                   <i class="i-Yes"></i>
                               </span>
                                   </button>
                               </div>

                               <div class="col-md-8">
                                   <div  style="display: flex;
align-items: center;
justify-content: left;
background: #f8f9fa;
border: 1px solid #eee;
border-radius: 20px;
position: relative;
width: 200px;
height: 35px;">
                                    <input type="text" placeholder="Search" style="background: transparent;
border: 0;
color: #212121;
font-size: .8rem;
line-height: 2;
height: 100%;
outline: initial !important;
padding: .5rem 1rem;
width: calc(100% - 32px);" v-model="searchKey" @input="emailSearch">
                                        <i class="search-icon text-muted i-Magnifi-Glass1" ></i>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <button type="button" class="btn btn-danger btn-icon rounded-circle m-1" style="width: 36px;
height: 36px;margin: 0; " @click="mailDelete()">
                                        <span class="ul-btn__icon">
                                        <i class="i-Close-Window"></i>
                                    </span>
                                    </button>
                                </div>

                           </div>
               </div>

                    <div class="mail-item"  v-if="!messages[0]">
                        <span class="spinner-glow spinner-glow-primary mr-5" style="margin-left: 141px;"></span>
                    </div>


                    <div class="mail-item " v-for="(msg,index) in messages"  v-else="messages[0]" @click="getSingleMessage(msg,index)">
                        {{--<span class="spinner-glow spinner-glow-primary mr-5" v-if="!message.sender"></span>--}}
                        <input type="checkbox"  @change="check($event,msg.messageId)" />
                        {{--<div @click="getSingleMessage(msg)">--}}
                        <div class="avatar">
                            {{--<img src="{{asset('/assets/images/faces/1.jpg')}}" alt="">--}}
                            <b v-html="avatar(msg.sender)"></b>
                        </div>
                        <div class="col-xs-6 details">
                            <b class="name text" v-text="msg.sender" style="color: #000"></b>
                            <p class="m-0" v-text="msg.subject"></p>
                        </div>
                        <div class="col-xs-3 date">
                            <span class="text-muted" v-text="receivedDate(msg.receivedTime)"></span>
                            <a href="javascript:;"  v-if="msg.flagid==='important'" style="float: right" title="Important"> <img src="{{url('assets/images/star.png')}}" alt="" width="16" /> </a>
                            <i class="i-Star"></i>
                        </div>
                        {{--</div>--}}
                        <div  :class="{ 'mail-active' : activeIndex == index}"></div>
                    </div>

                </div>
            </div>
        </div>

        <!-- MAIN INBOX SIDEBAR -->
        <div data-sidebar="main" data-sidebar-position="left" class="inbox-main-sidebar">
            <div class="pt-3 pr-3 pb-3">
                <i class="sidebar-close i-Close" data-sidebar-toggle="main"></i>
                <button class="btn btn-rounded btn-primary btn-block mb-4" @click="view_compose=1">{{__('message.compose') }}</button>
                <p class="text-muted mb-2" > <b>Browse</b> &nbsp; <span class="spinner-glow spinner-glow-primary mr-5" v-if="folders.length < 1"></span>  </p>
                <ul class="inbox-main-nav">

                    <li v-for="folder in folders" v-if="folders.length>0">
                        <a href="javascript:;" v-bind:class="[folder.folderId === selectedFolderId ? 'active' : '']" @click="loadFolderMessage(folder.folderId)" style="padding: .35rem 0;">
                            <i class="icon-regular i-Mail-2" v-if="folder.folderName=='Inbox'"></i>
                            <i class="icon-regular i-Mail-Outbox" v-else-if="folder.forlderName ==='Outbox' "></i>

                            <i class="icon-regular i-Mail-2" v-else></i>

                            <span v-text="folder.folderName"></span> </a></li>
                </ul>

                <hr>
                <p class="text-muted mb-2" > <b>Groupe
                {{-- <span style="float: right;"> <i class="icon-regular i-Add"></i> </span> --}}

                </b> </p>
                <ul class="inbox-main-nav">
                    <li v-for="group in groups"><a href=""><i class="icon-regular i-Folder"></i> <span v-text="group.name"></span> </a></li>
                </ul>
                <hr>
                <p class="text-muted mb-2" > <b>Syndicat  <span style="float: right;"> </a> </span></b> </p>
                <ul class="inbox-main-nav">
                </ul>
            </div>
        </div>

    </div>






@endsection


@section('page-js')

    <script>
        // $.fn.fileUploader = function (filesToUpload, sectionIdentifier) {
        //     var fileIdCounter = 0;
        //
        //     this.closest(".files").change(function (evt) {
        //         var output = [];
        //
        //         for (var i = 0; i < evt.target.files.length; i++) {
        //             fileIdCounter++;
        //             var file = evt.target.files[i];
        //             var fileId = sectionIdentifier + fileIdCounter;
        //
        //             filesToUpload.push({
        //                 id: fileId,
        //                 file: file
        //             });
        //
        //             var removeLink = "<a class=\"removeFile \" href=\"#\" data-fileid=\"" + fileId + "\"><i class='i-Close-Window'></i></a>";
        //
        //             output.push("<li class='btn btn-outline-secondary m-1'><strong>", escape(file.name), "</strong> &nbsp; ", removeLink, "</li> ");
        //         };
        //
        //         $(this).children(".fileList")
        //             .append(output.join(""));
        //
        //         //reset the input to null - nice little chrome bug!
        //         evt.target.value = null;
        //     });
        //
        //     $(this).on("click", ".removeFile", function (e) {
        //         e.preventDefault();
        //
        //         var fileId = $(this).parent().children("a").data("fileid");
        //
        //         // loop through the files array and check if the name of that file matches FileName
        //         // and get the index of the match
        //         for (var i = 0; i < filesToUpload.length; ++i) {
        //             if (filesToUpload[i].id === fileId)
        //                 filesToUpload.splice(i, 1);
        //         }
        //
        //         $(this).parent().remove();
        //     });
        //
        //     this.clear = function () {
        //         for (var i = 0; i < filesToUpload.length; ++i) {
        //             if (filesToUpload[i].id.indexOf(sectionIdentifier) >= 0)
        //                 filesToUpload.splice(i, 1);
        //         }
        //
        //         $(this).children(".fileList").empty();
        //     }
        //
        //     return this;
        // };
        //
        // (function () {
        //     var filesToUpload = [];
        //
        //     var files1Uploader = $("#files1").fileUploader(filesToUpload, "files1");
        // })()
    </script>

    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>
    <script src="{{asset('assets/js/vendor/quill.min.js')}}"></script>
    <script src="{{asset('assets/js/vendor/toastr.min.js')}}"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>
    <script type="text/javascript" src="{{asset('assets/js/vuejs/vue-resource.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/vuejs/axios.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/vuejs/vue.js')}}"></script>

    <script src="https://unpkg.com/vue2-editor/dist/vue2-editor.umd.min.js"></script>

    <script src="https://unpkg.com/vue/dist/vue.js"></script>
    <script src="https://unpkg.com/vue2-editor/dist/vue2-editor.umd.min.js"></script>


    <script>


        Vue.prototype.$http = axios;

        var vue = new Vue({
            el: '#app',
            data: {
                folders: [],
                groups: [],
                selectedFolderId : "5369298000000008014" ,
                // selectedFolderId : "5369298000000008" ,
                messages: [],
                singleMessage:{},
                email:{to: '', subject: '', cc:'', body: '', content: ''},
                view_compose: 2 ,  // 1:compose 2:single message,
                is_reply: null,
                first_message_id: null,
                message: {},
                replyEmail:{},
                forwardEmail:{},
                selectedIds: [],
                searchKey: null,
                file: [],
                attachments: [],
                activeIndex: 0,
                output: null

            },
            computed: {

                avatar(){
                    return fullName => fullName.split(' ').map(name => name[0]).join('').toUpperCase();
                }

            },
            created:{

            },
            mounted() {
            this.getFolderList();
            this.getGroupList();
            this.getMessageListByFolder();

            },
            methods: {
                fileUpload(){
                    // filesToUpload,
                    var filesToUpload = [];
                    let  fileIdCounter = 0;
                    let  sectionIdentifier = "files1" ;
                    var output = [];
                  var files =   this.$refs.file.files;
                  this.file =   this.$refs.file.files;
                    for (var i = 0; i < files.length; i++) {
                        fileIdCounter++;
                        var file = files[i];
                        var fileId = sectionIdentifier + fileIdCounter;

                        filesToUpload.push({
                            id: fileId,
                            file: file
                        });

                        // var removeLink = "<a class=\"removeFile \" href=\"javascript:;\" data-fileid=\"" + fileId + "\" @click=\"removeFile(fileId)\"><i class='i-Close-Window'></i></a>";

                        // output.push("<li class='btn btn-outline-secondary m-1'><strong>", escape(file.name), "</strong> &nbsp; ", removeLink, "</li> ");
                        output.push(file.name)
                    };

                    this.output = output; //output.join("")


                    // $(this).children(".fileList")
                    //     .append(output.join(""));

                    //reset the input to null - nice little chrome bug!
                    // evt.target.value = null;

                },
                removeFile(selectedFile){
                    this.output = this.output.filter(file => file !== selectedFile);
                },
                handleFileUpload(){
                    this.file = this.$refs.file.files[0];
                },
                makeMessageImportant(msgId){
                    var input = {messageId: msgId};

                    this.$http.post("{{url('api/v1/app/setmessageasimportant')}}", input).then(function (response) {
                        console.log("response", response);
                        if(response.status== 200){
                            toastr.success("This mail is mark as important", "Mail flag set", {
                                showDuration: 500
                            });
                            var vm = this
                            vm.loadFolderMessage(this.selectedFolderId);
                        }else{
                            toastr.error("Something went wrong.", "Error!", {
                                timeOut: 6000
                            })
                        }

                    }).catch(function (error) {
                        console.log(error);
                    });


                },
                emailSearch(){
                    var vm = this;
                    vm.messages = [];
                  let searchKey = vm.searchKey;
                    //get header data
                    if(searchKey.length>2){
                        this.$http.get("{{url('api/v1/app/searchresults')}}", {params: {searchKey: searchKey}}).then(function (response) {
                            let data = response.data.data;
                            let finalData = [];
                            console.log("selectedFolderId", vm.selectedFolderId);
                            if(vm.selectedFolderId === '6015833000000008020'){ //if selected folder is sent the need to swap
                                finalData =  data.forEach(item => {
                                    return {
                                        fromAddress: item.toAddress,
                                        subject: item.subject,
                                        body: item.content,
                                        toAddress: item.fromAddress,
                                        folderId: item.folderId,
                                        sender: item.sender,
                                        hasAttachment: item.hasAttachment
                                    }
                                });
                                data = finalData;
                            }
                            vm.$set(vm,'messages', data);
                            vm.getSingleMessage(response.data.data[0], 0);




                    }).catch(function (error) {
                        console.log(error);
                    });

                    }


                },
                check:function (event,id) {
                    if(this.selectedIds.includes(id)){
                       this.selectedIds =  this.selectedIds.filter(selectedId => selectedId != id);
                    }else{
                        this.selectedIds.push(id);
                    }
                },
                mailDelete:function () {
                    // var vm = this;
                    var input = {ids: this.selectedIds, folderId: this.selectedFolderId};
                    this.$http.post("{{url('api/v1/app/messagedelete')}}", input).then(function (response) {
                        console.log("response", response);
                        if(response.status== 200){
                            toastr.success("Mail has been successfully deleted!", "Mail Sent", {
                                showDuration: 500
                            });
                            var vm = this
                            vm.loadFolderMessage(this.selectedFolderId);
                        }else{
                            toastr.error("Something went wrong.", "Error!", {
                                timeOut: 6000
                            })
                        }

                    }).catch(function (error) {
                        console.log(error);
                    });

                },
                sendEmail: function () {
                    let input = new FormData();
                    if(this.file !== null && this.file.length> 0){
                        for(let i=0;i<this.file.length; i++){
                            input.append('fileName['+i+']', this.file[i]);
                        }

                    }
                    input.append('to', this.email.to);
                    input.append('subject', this.email.subject);
                    input.append('body', this.email.body);

                    // var input = this.email;
                    this.$http.post("{{url('api/v1/app/messages')}}", input, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }).then(function (response) {
                        if(response.status== 200){
                            this.file = [];
                            this.output =  [];
                        this.view_compose = null;
                        this.is_reply = null;
                            toastr.success("Mail has been successfully sent!", "Mail Sent", {
                                showDuration: 500
                            })
                        }else{
                            toastr.error("Something went wrong.", "Error!", {
                                timeOut: 6000
                            })
                        }

                    }).catch(function (error) {
                        console.log(error);
                    });

                },
                pushReplyMail: function () {
                    // var input = this.;
                    let input = new FormData();
                    // if(this.file !== null){
                    //     input.append('fileName', this.file);
                    // }
                    if(this.file !== null && this.file.length> 0){
                        for(let i=0;i<this.file.length; i++){
                            input.append('fileName['+i+']', this.file[i]);
                        }
                    }

                    input.append('toAddress', this.replyEmail.toAddress);
                    input.append('fromAddress', this.replyEmail.fromAddress);
                    input.append('subject', this.replyEmail.subject);
                    input.append('body', this.replyEmail.body);

                    this.$http.post("{{url('api/v1/app/pushReplyMail')}}/"+input.messageId, input).then(function (response) {
                        if(response.status == 200){
                            toastr.success("Mail has been successfully sent!", "Mail Sent", {
                                showDuration: 500
                            })
                            this.view_compose = null;
                            this.is_reply = null;

                        }else{
                            toastr.error("Something went wrong.", "Error!", {
                                timeOut: 6000
                            })
                        }

                    }).catch(function (error) {
                        console.log(error);
                    });

                },
                pushForwardMail:function () {
                    // var input = this.forwardEmail;
                    let input = new FormData();
                    // if(this.file !== null){
                    //     input.append('fileName', this.file);
                    // }

                    if(this.file !== null && this.file.length> 0){
                        for(let i=0;i<this.file.length; i++){
                            input.append('fileName['+i+']', this.file[i]);
                        }
                    }

                    input.append('to', this.forwardEmail.toAddress);
                    input.append('subject', this.forwardEmail.subject);
                    input.append('body', this.forwardEmail.body);

                    this.$http.post("{{url('api/v1/app/pushForwardMail')}}", input).then(function (response) {
                        if(response.status == 200){
                            toastr.success("Mail has been successfully sent!", "Mail Sent", {
                                showDuration: 500
                            })
                            this.view_compose = null;
                            this.is_reply = null;

                        }else{
                            toastr.error("Something went wrong.", "Error!", {
                                timeOut: 6000
                            })
                        }

                    }).catch(function (error) {
                        console.log(error);
                    });
                },

                receivedDate: function(dt){
                    const d = new Date(parseInt(dt));
                    const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
                    const mo = new Intl.DateTimeFormat('en', { month: 'short' }).format(d);
                    const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
                    return `${mo} ${da} , ${ye}`;

                },
                getFolderList: function () {
                    var vm = this;
                    //get header data
                    this.$http.get("{{url('api/v1/app/folders')}}").then(function (response) {
                        vm.$set(vm,'folders', response.data.data);
                        // vm.$set(vm,'selectedFolderId', response.data.data[0].folderId);

                    }).catch(function (error) {
                        console.log(error);
                    });
                },
                getGroupList: function () {
                    var vm = this;
                    //get header data
                    this.$http.get("{{url('api/v1/app/groups')}}").then(function (response) {
                        vm.$set(vm,'groups', response.data.data.groups);
                    }).catch(function (error) {
                        console.log(error);
                    });
                },
                loadFolderMessage: function(folderId){
                    this.selectedFolderId = folderId;
                    this.getMessageListByFolder();
                },
                getMessageListByFolder: function () {
                    var vm = this;
                    //get header data
                    this.$http.get("{{url('api/v1/app/messagesbyfolder')}}/" + vm.selectedFolderId).then(function (response) {
                    let data = response.data.data;
                    let finalData = [];
                    console.log("selectedFolderId", vm.selectedFolderId);
                        if(vm.selectedFolderId === '6015833000000008020'){ //if selected folder is sent the need to swap
                            finalData =  data.forEach(item => {
                            return {
                                fromAddress: item.toAddress,
                                subject: item.subject,
                                body: item.content,
                                toAddress: item.fromAddress,
                                folderId: item.folderId,
                                sender: item.sender,
                                hasAttachment: item.hasAttachment
                            }
                            });
                            data = finalData;
                        }
                        vm.$set(vm,'messages', data);

                        // console.log("response.data.data[0].messageId", response.data.data);
                        // console.log("response.data.data[0].messageId", response.data.data[0].messageId);
                        // vm.$set(vm, 'first_message_id', response.data.data[0].messageId);
                        vm.getSingleMessage(response.data.data[0], 0);

                    }).catch(function (error) {
                        console.log(error);
                    });
                },
                getSingleMessage: function(message, activeIndex){
                    var vm = this;
                    vm.activeIndex = activeIndex;
                    vm.$set(vm,'singleMessage', {});


                    vm.view_compose = 2;
                    vm.$set(vm,'message', message);

                    vm.replyEmail.subject =  'Re : ' +  message.subject;
                    vm.replyEmail.fromAddress = message.toAddress;
                    vm.replyEmail.toAddress = message.fromAddress;
                    vm.replyEmail.messageId = message.messageId;
                    vm.replyEmail.body = message.content;
                    vm.forwardEmail.fromAddress = message.toAddress;
                    vm.forwardEmail.subject =  'Fwd : ' +  message.subject;
                    //get header data
                    this.$http.get("{{url('api/v1/app/messages/singlemessage')}}/"+ vm.selectedFolderId + "/"+ message.messageId).then(function (response) {
                        vm.$set(vm,'singleMessage', response.data.data);
                        vm.$set(vm,'attachments', response.data.attachments.attachments);
                        vm.forwardEmail.body = response.data.data.content
                    }).catch(function (error) {
                        console.log(error);
                    });
                }

            } // end of methods

        })

    </script>


@endsection



@section('bottom-js')

    <script src="{{asset('assets/js/sidebar.script.js')}}"></script>

@endsection