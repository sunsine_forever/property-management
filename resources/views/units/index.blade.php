@extends('layouts.master')

@section('page-css')

    <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection


@section('main-content')
    @include('partials.flash-message')

    <div class="breadcrumb">
        <h1>{{$pageTitle}}</h1>
        {{--<ul>--}}
        {{--<li><a href="">Dashboard</a></li>--}}
        {{--<li>List</li>--}}
        {{--</ul>--}}
    </div>
    <div class="separator-breadcrumb border-top"></div>

    <a href="javascript:;" class="btn  btn-outline-dark btn-sm m-1"> Export </a>
    <a href="{{url('building/units/'.$buildingId.'/add')}}" class="btn btn-success btn-sm m-1"> <i class="i-Add"></i> Unit </a>


    <div class="row">
        <!-- start col-md-12 -->
        <div class="col-md-12">

            <div class="card text-left">

                <div class="card-body">
                    <h4 class="card-title mb-3">Units</h4>

                    <div class="table-responsive">

                        <table id="sample_1" class="display table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr class="uppercase">
                                <th> #  </th>
                                <th> Unit attribute  </th>
                                <th> Room  </th>
                                <th>  Code </th>
                                <th> Rent  </th>
                                <th> Misc.  </th>
                                <th></th>

                            </tr>

                            </thead>
                        </table>
                    </div>

                </div>
            </div> <!-- end col-md-12 -->
        </div>

    </div>

@endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    {{--<script src="{{asset('assets/js/datatables.script.js')}}"></script>--}}



    <script>

        $(document).ready(function() {

            $('#sample_1').DataTable( {
                "bStateSave": true,

                // save custom filters to the state
                "fnStateSaveParams":    function ( oSettings, sValue ) {
                    $("#datatable_ajax tr.filter .form-control").each(function() {
                        sValue[$(this).attr('name')] = $(this).val();
                    });

                    return sValue;
                },

                // read the custom filters from saved state and populate the filter inputs
                "fnStateLoadParams" : function ( oSettings, oData ) {
                    //Load custom filters
                    $("#datatable_ajax tr.filter .form-control").each(function() {
                        var element = $(this);
                        if (oData[element.attr('name')]) {
                            element.val( oData[element.attr('name')] );
                        }
                    });

                    return true;
                },
                "processing": true,
                "serverSide": true,
                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
                "ajax": {
                    "url": "{{url('ajax/units')}}?buildingId={{$buildingId}}",
                    "type": "POST"
                },
                "columns": [
                    { "data":'row_num',"orderable":false},
                    { "data": function (data, type, dataToSet) {
                        return 'Floor: ' + data.floor + '<br> Type: ' + data.type + '<br> Square feet : ' + data.square_feet +
                            '<br> Square feet balcony: ' + data.square_feet_balcony + '<br> Percent square footage: ' + data.percent_square_footage
                        }, "orderable": false},
                    { "data": function (data, type, dataToSet) {
                            return 'Rooms: ' + data.rooms + '<br> Plan: '+ data .plan ;
                        }, "orderable": false },

                    { "data": function (data, type, dataToSet) {
                            var address  =  'Condo Fees: ' + data.condo_fees + '<br> Intercom Code : '+ data.intercom_code + '<br> Buzz code: ' + data.buzz_code + '<br> Wifi password: '+ data.wifi_password
                            return address;
                        }, "orderable": false},
                    { "data": function(data, type, dataToSey){
                            var rent =  'Rent Amount ' + data.rent_amount + '<br> Rent Start : ' + data.rent_start + '<br>Rent End: ' + data.rent_end  + '<br> Lease: ' + data.lease;
                            const alarm  =   (data.is_private) ? 'Yes' : 'No';
                            return rent + '<br> Alarm: '+ alarm;
                        }, "orderable": false},
                    { "data": function (data, type, dataToSet) {
                            return 'Annexes: ' + data.annexes + '<br> Reference: ' +  data.reference ;
                        }, "orderable": false},
                    { "data": "row_id", "orderable":false, "render": function (data) {
                            var string = '<button type="button" class="btn bg-white _r_btn border-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                                '<span class="_dot _inline-dot bg-primary"></span>' +
                                '<span class="_dot _inline-dot bg-primary"></span>' +
                                '<span class="_dot _inline-dot bg-primary"></span>  ' +
                                '</button>  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 33px, 0px); top: 0px; left: 0px; will-change: transform;">'

                            string = string + '<a class="dropdown-item ul-widget__link--font" href="{{ url('building/units/'.$buildingId.'/edit/') }}/'+ data + '">  <i class="i-Pen-2"> </i> Edit </a>  </div>';
                            return string
                        }
                    }

                ],
                // "order": [0]// set first column as a default sort by asc
                "order": [[ 0, "desc" ]]
            } );
        } );


    </script>

@endsection
