@extends('layouts.master')
@section('before-css')
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection
@section('page-css')

    <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection


@section('main-content')

    @include('partials.errors')
    @include('partials.flash-message')


    <div class="breadcrumb">
        <h1>{{$pageTitle}}</h1>

    </div>

    <div class="separator-breadcrumb border-top"></div>
    <div class="row">

        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="card-title mb-3">{{$pageTitle}} <span style="color: #969696;">- {{$subBtnText}}</span></div>
                    @if(isset($unit))
                        <form method="post" action="{{ url('building/units/'.$buildingId.'/update/'.$unit->id) }}" class="needs-validation" novalidate>
                            @method('PATCH')

                            @else
                                <form action="{{ url('building/units/'. $buildingId.'/save') }}" method="POST" class="needs-validation" novalidate>
                                    @endif
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="floor">Floor</label>
                                            <input type="text" class="form-control" id="floor" placeholder="Enter floor" required value="{{{$unit->floor ?? '' }}}" name="floor" >

                                            <div class="invalid-feedback">
                                                Please provide a valid  floor.
                                            </div>
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="type">Type </label>
                                            <input type="text" class="form-control" id="type" placeholder="Enter type"  value="{{{$unit->type ?? '' }}}" name="type">
                                            <div class="invalid-feedback">
                                                Please provide a brand type.
                                            </div>
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="square_feet">Square Feet </label>
                                            <input type="text" class="form-control" id="square_feet" placeholder="Square feet" value="{{{$unit->square_feet ?? '' }}}" name="square_feet">
                                            <div class="invalid-feedback">
                                                Please provide a square_feet.
                                            </div>
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="square_feet_balcony">Square feet balcony</label>
                                            <input class="form-control" id="square_feet_balcony" placeholder="Square feet balcony" name="square_feet_balcony" value="{{{$unit->square_feet_balcony ?? '' }}}">
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="percent_square_footage">Percent Square Footage</label>
                                            <input class="form-control" id="percent_square_footage" placeholder="Card" name="percent_square_footage" value="{{{$unit->percent_square_footage ?? '' }}}">

                                            {{--@error('horizontal_logo')--}}
                                            {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                            {{--@enderror--}}

                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="rooms">Rooms</label>
                                            <input type="number" class="form-control" id="province" placeholder="Rooms"  name="rooms" value="{{{$unit->rooms ?? '' }}}">
                                            <div class="invalid-feedback">
                                                Please provide a valid province.
                                            </div>
                                        </div>

                                        <div class="col-md-12 form-group mb-3">
                                            <label for="note">Note</label>
                                            <textarea name="note" id="" cols="30" rows="10" class="form-control"> {{{$unit->note ?? '' }}} </textarea>
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="plan">Plan</label>
                                            <input type="text" class="form-control" id="plan" placeholder="Enter plan" name="plan" required value="{{{$unit->plan ?? '' }}}">
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="condo_fees">Condo Fees</label>
                                            <input type="text" class="form-control" id="condo_fees" placeholder="Conod Fees" name="condo_fees" value="{{{$unit->condo_fees ?? '' }}}">
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="intercom_code">Intercom code</label>
                                            <input type="text" class="form-control" id="intercom_code" placeholder="Interncom code" required name="intercom_code" value="{{{$unit->intercom_code ?? '' }}}">
                                            <div class="invalid-feedback">
                                                Please provide a valid intercom code.
                                            </div>
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="buzz_code">Buz code</label>
                                            <input type="text" class="form-control" id="buzz_code" placeholder="Buz code" name="buzz_code" value="{{{$unit->buzz_code ?? '' }}}">
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="wifi_password">Wifi password </label>
                                            <input type="text" class="form-control" id="wifi_password" placeholder="Wifi password" name="wifi_password"  value="{{{$unit->wifi_password ?? '' }}}">
                                        </div>

                                        <div class="col-md-2 form-group mb-1">
                                            <label for="has_alarm">&nbsp;</label>
                                            <label class="checkbox checkbox-outline-primary">
                                                <input type="checkbox" name="has_alarm" value="{{{$unit->has_alarm ?? '' }}}" @if(isset($unit->has_alarm)) checked @endif >
                                                <span>Alarm</span>
                                                <span class="checkmark"></span>
                                            </label>

                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="rent_amount"> Rent amount </label>
                                            <input type="text" class="form-control" id="rent_amount" placeholder=" Rent amount" required name="rent_amount" value="{{{$unit->rent_amount ?? '' }}}">
                                            <div class="invalid-feedback">
                                                Please provide a valid name.
                                            </div>
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="lease"> Lease </label>
                                            <input type="text" class="form-control" id="lease" placeholder="Lease" required name="lease" value="{{{$unit->lease ?? '' }}}">
                                            <div class="invalid-feedback">
                                                Please provide a valid name.
                                            </div>
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="rent_start"> Rent start </label>
                                            <input type="text" class="form-control" id="rent_amount" placeholder="YYYY-MM-DD" required name="rent_start" value="{{{$unit->rent_start ?? '' }}}">
                                            <div class="invalid-feedback">
                                                Please provide a valid name.
                                            </div>
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="rent_end"> Rent end </label>
                                            <input type="text" class="form-control" id="rent_end" placeholder="YYYY-MM-DD" required name="rent_end" value="{{{$unit->rent_end ?? '' }}}">
                                            <div class="invalid-feedback">
                                                Please provide a valid name.
                                            </div>
                                        </div>



                                        <div class="col-md-12 form-group mb-3">
                                            <label for="annexes">Annexes</label>
                                            <textarea name="annexes" id="" cols="30" rows="10" class="form-control"> {{{$unit->annexes ?? '' }}} </textarea>
                                        </div>

                                        <div class="col-md-12 form-group mb-3">
                                            <label for="reference">Reference</label>
                                            <textarea name="reference" id="" cols="30" rows="10" class="form-control"> {{{$unit->reference ?? '' }}} </textarea>
                                        </div>

                                        <br>
                                        <div class="col-md-12">
                                            <button class="btn btn-primary">{{$subBtnText}}</button>
                                            <a href="{{url('building/units/' . $buildingId)}}" class="btn btn-light"> Cancel</a>
                                        </div>

                                    </div>
                                </form>
                </div>
            </div>
        </div>
    </div>



@endsection

@section('page-js')

@endsection
@section('bottom-js')
    <script src="{{asset('assets/js/form.basic.script.js')}}"></script>
    <script src="{{asset('assets/js/form.validation.script.js')}}"></script>


@endsection