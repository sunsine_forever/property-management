@extends('layouts.master')
@section('before-css')
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection
@section('page-css')

    <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection


@section('main-content')

    @include('partials.errors')
    @include('partials.flash-message')


    <div class="breadcrumb">
        <h1>{{$pageTitle}}</h1>
        {{--<ul>--}}
            {{--<li><a href="">Dashboard</a></li>--}}
            {{--<li>Add Building</li>--}}
        {{--</ul>--}}
    </div>

    <div class="separator-breadcrumb border-top"></div>
    <div class="row">

        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="card-title mb-3">Building</div>
                    @if(isset($building))
                        <form method="post" action="{{ url('manage/building/update/'.$building->id) }}" class="needs-validation" novalidate>
                            @method('PATCH')

                    @else
                                <form action="{{ url('manage/building/save') }}" method="POST" class="needs-validation" novalidate>
                    @endif
                            @csrf
                        <div class="row">
                            <div class="col-md-6 form-group mb-3">
                                <label for="project_name">Project name</label>
                                <input type="text" class="form-control" id="project_name" placeholder="Enter project name" required value="{{{$building->project_name ?? '' }}}" name="project_name" >

                                <div class="invalid-feedback">
                                    Please provide a valid project name.
                                </div>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="brand_color">Brand color </label>
                                <input type="text" class="form-control" id="brand_color" placeholder="Enter brand color"  value="{{{$building->brand_color ?? '' }}}" name="brand_color">
                                <div class="invalid-feedback">
                                    Please provide a brand color.
                                </div>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="project_initial">Project initial</label>
                                <input type="text" class="form-control" id="project_initial" placeholder="Project initial" value="{{{$building->project_initial ?? '' }}}" name="project_initial">
                                <div class="invalid-feedback">
                                    Please provide a project initial.
                                </div>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="phone">Project avatar</label>
                                <input class="form-control" id="phone" placeholder="Project avatar" name="project_avatar">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="credit1">Horizotal logo</label>
                                <input class="form-control" id="credit1" placeholder="Card" name="horizontal_logo">

                                {{--@error('horizontal_logo')--}}
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                {{--@enderror--}}

                            </div>

                           <div class="col-md-12">
                               <hr>
                               <h4> Address </h4>

                           </div>


                            <div class="col-md-6 form-group mb-3">
                                <label for="country">Country</label>
                                {!! $country !!}
                                {{--<input class="form-control" id="credit1" placeholder="Country"  name="country" value="{{{$building->address->country ?? '' }}}">--}}
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="province">Province</label>
                                <input type="text" class="form-control" id="province" placeholder="Province"  name="province" value="{{{$building->address->province ?? '' }}}">
                                <div class="invalid-feedback">
                                    Please provide a valid province.
                                </div>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="province_code">Province code</label>
                                <input type="text" class="form-control" id="province_code" placeholder="Enter your last name" name="province_code" required value="{{{$building->address->province_code ?? '' }}}">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="city">City</label>
                                <input type="text" class="form-control" id="city" placeholder="Enter city" name="city" value="{{{$building->address->city ?? '' }}}">
                                <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                            </div>


                            <div class="col-md-6 form-group mb-3">
                                <label for="firstName1">Address line 1</label>
                                <input type="text" class="form-control" id="firstName1" placeholder="Address line 1" required name="address_line1" value="{{{$building->address->address_line1 ?? '' }}}">
                                <div class="invalid-feedback">
                                    Please provide a valid name.
                                </div>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="address_line2">Address line 2</label>
                                <input type="text" class="form-control" id="address_line2" placeholder="Address line 2" name="address_line2" value="{{{$building->address->address_line2 ?? '' }}}">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="postal_code">Postal code </label>
                                <input type="text" class="form-control" id="postal_code" placeholder="Postal code" name="postal_code"  value="{{{$building->address->postal_code ?? '' }}}">
                            </div>



                            <div class="col-md-12">
                                <hr>
                                <h4> Info </h4>

                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="number_unit"> Number unit </label>
                                <input type="number" class="form-control" id="number_unit" placeholder="Enter your first name" required name="number_unit" value="{{{$building->info->number_unit ?? '' }}}">
                                <div class="invalid-feedback">
                                    Please provide a valid name.
                                </div>
                            </div>

                            <div class="col-md-2 form-group mb-1">
                                <label for="lastName1">&nbsp;</label>
                                <label class="checkbox checkbox-outline-primary">
                                    <input type="checkbox" name="is_condo" value="{{{$building->info->is_condo ?? '' }}}" @if(isset($building->info->is_condo)) checked @endif >
                                    <span>Condo</span>
                                    <span class="checkmark"></span>
                                </label>

                            </div>
                            <div class="col-md-2 form-group mb-1">
                                <label for="lastName1">&nbsp;</label>
                                <label class="checkbox checkbox-outline-primary">
                                    {{--<input type="checkbox" name="is_rental">--}}
                                    <input type="checkbox" name="is_rental" value="{{{$building->info->is_rental ?? '' }}}" @if(isset($building->info->is_rental)) checked @endif >
                                    <span>Rental</span>
                                    <span class="checkmark"></span>
                                </label>

                            </div>

                            <div class="col-md-2 form-group mb-1">
                                <label for="lastName1">&nbsp;</label>
                                <label class="checkbox checkbox-outline-primary">
                                    <input type="checkbox" name="is_construction" value="{{{$building->info->is_construction ?? '' }}}" @if(isset($building->info->is_construction)) checked @endif >
                                    <span>Construction</span>
                                    <span class="checkmark"></span>
                                </label>

                            </div>


                            <div class="col-md-12 form-group mb-3">
                                <label for="exampleInputEmail1">Information</label>
                                <textarea name="add_info" id="" cols="30" rows="10" class="form-control"> {{{$building->info->add_info ?? '' }}} </textarea>
                                <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                            </div>

                            {{--<div class="col-md-6 form-group mb-3">--}}
                                {{--<label for="phone">Phone</label>--}}
                                {{--<input class="form-control" id="phone" placeholder="Enter phone">--}}
                            {{--</div>--}}



                            <div class="col-md-12">
                                <hr>
                                <h4> Contact </h4>


                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="first_name">First name</label>
                                <input type="text" class="form-control" id="first_name" placeholder="Enter your first name" required name="first_name" value="{{{$building->contact->first_name ?? '' }}}">
                                <div class="invalid-feedback">
                                    Please provide a valid name.
                                </div>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="last_name">Last name</label>
                                <input type="text" class="form-control" id="last_name" placeholder="Enter your last name" name="last_name" value="{{{$building->contact->last_name ?? '' }}}">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" value="{{{$building->contact->email ?? '' }}}" name="email">
                                <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="phone_number">Phone</label>
                                <input class="form-control" id="phone_number" placeholder="Enter phone" name="phone_number" value="{{{$building->contact->phone_number ?? '' }}}">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="phone_ext">Phone ext.</label>
                                <input class="form-control" id="phone_ext" placeholder="Card" name="phone_ext" value="{{{$building->contact->phone_ext ?? '' }}}">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="website">Website</label>
                                <input type="text" class="form-control" id="website" placeholder="Enter website" name="website" value="{{{$building->contact->website ?? '' }}}">

                            </div>

                            <div class="col-md-2 form-group mb-1">
                                <label class="checkbox checkbox-outline-primary">
                                    <input type="checkbox" name="is_private" value="{{{$building->contact->is_private ?? '' }}}" @if(isset($building->contact->is_private)) checked @endif >
                                    <span>Private</span>
                                    <span class="checkmark"></span>
                                </label>

                            </div>
                            <br>
                            <div class="col-md-12">
                                <button class="btn btn-primary">{{$subBtnText}}</button>
                                <a href="{{url('manage/building/index')}}" class="btn btn-light"> Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>



@endsection

@section('page-js')

@endsection
@section('bottom-js')
    <script src="{{asset('assets/js/form.basic.script.js')}}"></script>
    <script src="{{asset('assets/js/form.validation.script.js')}}"></script>


@endsection