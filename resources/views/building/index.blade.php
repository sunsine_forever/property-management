@extends('layouts.master')

@section('page-css')

    <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection


@section('main-content')
    @include('partials.flash-message')

    <div class="breadcrumb">
        <h1>Building</h1>
        {{--<ul>--}}
            {{--<li><a href="">Dashboard</a></li>--}}
            {{--<li>List</li>--}}
        {{--</ul>--}}
    </div>
    <div class="separator-breadcrumb border-top"></div>

    <a href="javascript:;" class="btn  btn-outline-dark btn-sm m-1"> Export </a>
    <a href="{{url('manage/building/add')}}" class="btn btn-success btn-sm m-1"> <i class="i-Add"></i> Building</a>


    <div class="row">
        <!-- start col-md-12 -->
        <div class="col-md-12">

            <div class="card text-left">

                <div class="card-body">
                    <h4 class="card-title mb-3">Buildings</h4>

                <div class="table-responsive">

                        <table id="sample_1" class="display table table-striped table-bordered" style="width:100%">
                        <thead>
                        <tr class="uppercase">
                            <th> #  </th>
                            <th> Avatar  </th>
                            <th> Project  </th>
                            <th>  Address </th>
                            <th> Contact  </th>
                            <th> Info  </th>
                            <th></th>

                        </tr>

                        {{--<tr role="row" class="filter">--}}
                            {{--<td class="padding">--}}
                            {{--</td>--}}
                            {{--<td class="padding">--}}
                            {{--</td>--}}
                            {{--<td class="padding">--}}
                                {{--<input type="text" class="form-control form-filter input-sm" name="project_name" placeholder="Project Name">--}}
                            {{--</td>--}}
                            {{--<td class="padding">--}}
                                {{--<input type="text" class="form-control form-filter input-sm" name="brand_color" placeholder="Brand Color">--}}
                            {{--</td>--}}
                            {{--<td class="padding">--}}
                                {{--<input type="text" class="form-control form-filter input-sm" name="project_initial" placeholder="Project Initial">--}}
                            {{--</td>--}}
                            {{--<td class="padding">--}}
                                {{--<input type="text" class="form-control form-filter input-sm" name="" placeholder="" disabled>--}}
                            {{--</td>--}}

                            {{--<td>--}}
                                {{--<div class="margin-bottom-5">--}}
                                    {{--<button class="btn btn-info btn-sm m-1" style="width: 100%">--}}
                                        {{--<i class="fa fa-search"></i> Search</button>--}}
                                {{--</div>--}}
                                {{--<button class="btn btn-danger btn-sm m-1" style="width: 100%">--}}
                                    {{--<i class="fa fa-times"></i> Reset</button>--}}
                            {{--</td>--}}

                        {{--</tr>--}}

                        </thead>
                    </table>
                </div>

            </div>
        </div> <!-- end col-md-12 -->
    </div>

    </div>

@endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    {{--<script src="{{asset('assets/js/datatables.script.js')}}"></script>--}}



    <script>

        $(document).ready(function() {

            $('#sample_1').DataTable( {
                "bStateSave": true,

                // save custom filters to the state
                "fnStateSaveParams":    function ( oSettings, sValue ) {
                    $("#datatable_ajax tr.filter .form-control").each(function() {
                        sValue[$(this).attr('name')] = $(this).val();
                    });

                    return sValue;
                },

                // read the custom filters from saved state and populate the filter inputs
                "fnStateLoadParams" : function ( oSettings, oData ) {
                    //Load custom filters
                    $("#datatable_ajax tr.filter .form-control").each(function() {
                        var element = $(this);
                        if (oData[element.attr('name')]) {
                            element.val( oData[element.attr('name')] );
                        }
                    });

                    return true;
                },
                "processing": true,
                "serverSide": true,
                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
                "ajax": {
                    "url": "{{url('ajax/building')}}",
                    "type": "POST"
                },
                "columns": [
                    { "data":'row_num',"orderable":false},
                    { "data":'project_avatar', "render": function (data) {
                            return '<img src="{{url('storage/images')}}/' + data + '" width="90">';
                        }, "orderable": false},
                    { "data": function (data, type, dataToSet) {
                            var  project = 'Name: ' + data.project_name + "<br>" + " Initial : " + data.project_initial + ' <br> Color code: ' + data.brand_color +  '<br> <span style=" background-color: '+ data.brand_color +';color: #ffffff; text-transform: uppercase; " class="badge m-2"> Brand color </span>';
                        return project;
                        }, "orderable": false },
                    { "data": function (data, type, dataToSet) {
                            var address  =  'Country: ' + data.country + '<br> Province : '+ data.province + '<br> province code: ' + data.province_code + '<br> City: '+ data.city
                            return address;
                        }, "orderable": false},
                    { "data": function(data, type, dataToSey){
                        var contact =  'Name ' + data.first_name + ' ' + data.last_name + '<br> Email: ' + data.email + '<br>Phone: ' + data.phone_number + '<br> Website: '+ data.website  ;
                         const private  =   (data.is_private) ? 'Yes' : 'No';
                         return contact + '<br> Private: '+ private;
                        }, "orderable": false},
                    { "data": function (data, type, dataToSet) {
                            var info =  ' Number unit: ' + data.number_unit;
                            const condo = (data.is_condo)? 'Yes': 'No';
                            const rental = (data.is_rental)? 'Yes': 'No';
                            const construction =  (data.is_construction)? 'Yes': 'No';
                            return info + '<br> Condo: ' + condo + '<br> Rental: ' +  rental + '<br> Construction: ' + construction ;
                        }, "orderable": false},
                    { "data": "row_id", "orderable":false, "render": function (data) {
                        var string = '<button type="button" class="btn bg-white _r_btn border-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                            '<span class="_dot _inline-dot bg-primary"></span>' +
                            '<span class="_dot _inline-dot bg-primary"></span>' +
                            '<span class="_dot _inline-dot bg-primary"></span>  ' +
                            '</button>  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 33px, 0px); top: 0px; left: 0px; will-change: transform;">'

                            string = string + '<a class="dropdown-item ul-widget__link--font" href="{{ url('manage/building/edit/') }}/'+ data + '">  <i class="i-Pen-2"> </i> Edit </a>  </div>';
                            return string
                        }
                    }

                ],
                // "order": [0]// set first column as a default sort by asc
                "order": [[ 0, "desc" ]]
            } );
        } );


    </script>

@endsection
