@extends('layouts.master')
@section('page-css')
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/quill.bubble.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/quill.snow.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/sweetalert2.min.css')}}">
@endsection


@section('main-content')
    @include('partials.flash-message')

    <div class="breadcrumb">
        <h1>{{$pageTitle}}</h1>

    </div>
    <div class="separator-breadcrumb border-top"></div>


    <div class="row">
        <div class="col-md-10"></div>
        <div class="col-md-2">

            <a href="javascript:;" class="btn btn-success btn-sm m-1" style="float: right;" data-ticketId="{{$ticket->id}}" id="alert-confirm-delete">  Delete </a>
            <a href="javascript:;" class="btn  btn-outline-dark btn-sm m-1" style="float: right;" data-ticketId="{{$ticket->id}}" id="alert-confirm-close"> Close </a>
        </div>
    </div>


    <div class="row">
        <div class="col-md-10">
            <h4> Ticket # {{$ticket->ticketNumber}} - {{$ticket->subject}} - {{$ticket->priority}}</h4>
            <h5> {{$ticket->assignee->firstName. ' ' . $ticket->assignee->lastName}} ({{$ticket->assignee->email}}) - {{$ticket->department->name}} </h5>
        </div>
        <div class="col-md-2">
            {{$ticket->status}}

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @foreach($comments as $comment)
            <div class="card mb-4">
                <div class="card-header" style="background-color: #000; color: #FFF;font-size: 12px ">
                    <span style="text-align: left;font-size: 14px;">{{$comment->commenter->firstName. ' ' . $comment->commenter->lastName }}</span>

                    <div style="float:right;">
                        <span>
                               <i class="i-Calendar-4"></i>
                            {{date('Y/m/d', strtotime($comment->commentedTime))}}
                        </span>
                        &nbsp; &nbsp;
                        <span>
                               <i class="i-Clock-Forward"></i>
                            {{date('h:i a ', strtotime($comment->commentedTime))}}
                        </span>

                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title">{!! $comment->content !!}</h5>
                </div>
            </div>
                @endforeach
        </div>

    </div>



    <div class="row">
        <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-header" style="background-color: #4caf50; color: #FFF;font-size: 12px ">
                        <span style="text-align: left;font-size: 14px;"> Reply to ticket #{{$ticket->ticketNumber}}</span>

                    </div>
                    <div class="card-body">
                        <form action="{{ url('tickets/'. $ticketId .'/comments') }}" method="POST" class="needs-validation" novalidate>

                            {{--@endif--}}
                            @csrf
                            <div class="row">
                                <div class="col-md-12 form-group mb-3">
                                    <input type="text" id="full-editor" placeholder="Comment ..." required  name="commentContent" style="width: 100%">


                                </div>
                                <br>
                                <br>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-3" style="margin-top: 50px">
                                    <button class="btn btn-success" style="float: right;">Soumettre</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
        </div>

    </div>


@endsection

@section('page-js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
    <script src="{{asset('assets/js/vendor/quill.min.js')}}"></script>
    <script src="{{asset('assets/js/vendor/sweetalert2.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            var quill = new Quill('#full-editor', {
                modules: {
                    syntax: !0,
                    toolbar: [
                        [{
                            font: []
                        }, {
                            size: []
                        }],
                        ["bold", "italic", "underline", "strike"],
                        [{
                            color: []
                        }, {
                            background: []
                        }],
                        [{
                            script: "super"
                        }, {
                            script: "sub"
                        }],
                        [{
                            header: "1"
                        }, {
                            header: "2"
                        }, "blockquote", "code-block"],
                        [{
                            list: "ordered"
                        }, {
                            list: "bullet"
                        }, {
                            indent: "-1"
                        }, {
                            indent: "+1"
                        }],
                        ["direction", {
                            align: []
                        }],
                        ["link", "image", "video", "formula"],
                        ["clean"]
                    ]
                },
                theme: 'snow'
            });
            var quill = new Quill('#snow-editor', {
                modules: {


                    syntax: !0,


                },
                theme: 'snow'
            });

            var quill = new Quill('#bubble-editor', {
                modules: {


                    syntax: !0,


                },
                theme: 'bubble'
            });
        });

        //Delete ticket
        $('#alert-confirm-delete').on('click', function () {

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0CC27E',
                cancelButtonColor: '#FF586B',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success mr-5',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(
                function () {
                    $.ajax({
                        method:     "GET",
                        {{--url: "{{URL::to('ajax/tickets/delete')}}",--}}
                        url: "{{URL::to('tickets/movetotrash/'.$ticket->id )}}" ,
                        dataType:"HTML",
                        success: function(result){
                            swal(
                                'Deleted!',
                                'Ticket has been deleted.',
                                'success'
                            )
                            // Simulate a mouse click:
                            window.location.href = "{{url('tickets')}}";
                        },
                        error: function(results){
                            swal(
                                'Error',
                                results,
                                'error'
                            )
                        }
                    });

                }, function (dismiss) {
                    if (dismiss === 'cancel') {
                        swal(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        )
                    }
                }
            )
        });
        $('#alert-confirm-close').on('click', function () {
            ticketId = $(this).attr('data-ticketId');

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0CC27E',
                cancelButtonColor: '#FF586B',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success mr-5',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(
                function () {
                    $.ajax({
                        method:     "GEt",
                        {{--url: "{{URL::to('ajax/tickets/delete')}}",--}}
                        url: "{{URL::to('tickets/close/' . $ticket->id )}}" ,
                        data: {ticketId: ticketId },
                        dataType:"HTML",
                        success: function(result){
                            swal(
                                'Close!',
                                'Ticket has been close.',
                                'success'
                            )
                            location.reload();
                        },
                        error: function(results){
                            swal(
                                'Error',
                                results,
                                'error'
                            )
                        }
                    });

                }, function (dismiss) {
                    if (dismiss === 'cancel') {
                        swal(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        )
                    }
                }
            )
        });

    </script>
@endsection



