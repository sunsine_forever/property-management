@extends('layouts.master')
@section('page-css')
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/sweetalert2.min.css')}}">
@endsection


@section('main-content')
    @include('partials.flash-message')

    <div class="breadcrumb">
        <h1>{{$pageTitle}}</h1>

    </div>
    <div class="separator-breadcrumb border-top"></div>


    <div class="row">
        <div class="col-md-12 mb-3">
            <div class="card text-left">

                <div class="card-body">
                    {{--<h4 class="card-title mb-3"> Dark Table Head</h4>--}}

                    <div class="table-responsive">
                        <table class="table ">
                            <thead class="thead-dark">

                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Priorité</th>
                                <th scope="col"></th>
                                <th scope="col">Sujet</th>
                                <th scope="col">Resident</th>
                                <th scope="col">Syndicat</th>
                                <th scope="col">Unite</th>
                                <th scope="col">Status</th>
                                <th scope="col">Comment</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($rows as $key=> $row)
                            <tr>
                                <th scope="row">{{$row->ticketNumber }}</th>
                                <td>{{$row->priority}}</td>
                                <td>{{ date('d/m/Y',strtotime($row->createdTime)) }}</td>
                                <td>{{$row->subject}}</td>
                                <td>{{$row->assignee->firstName. ' ' . $row->assignee->lastName}}</td>
                                <td>{{$row->customFields->Building}}</td>
                                <td>{{$row->customFields->Unit}}</td>

                                <td>
                                    {{--{{$row->status}}--}}
                                    @if($row->status == 'Closed')
                                        <span class="badge badge-success">Fermé</span>
                                        @elseif($row->status == 'On Hold')
                                        {{--<span class="badge badge-danger">On Hold</span>--}}
                                        <span class="badge badge-danger">En attente</span>
                                        @else
                                        <span class="badge badge-info">{{$row->status}}</span>
                                        @endif
                                </td>
                                <td>{{$row->commentCount}}</td>
                                <td>
                                    <button class="btn btn-outline-danger m-1" type="button" id="alert-confirm" data-ticketId="{{$row->id}}">Delete</button>

                                    <a class="btn btn-outline-info m-1" type="button" href="{{url("tickets/". $row->id. "/comments")}}">Comment(s)</a>

                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>
        <!-- end of col-->
    </div>

@endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/sweetalert2.min.js')}}"></script>
    <script>
        $('#alert-confirm').on('click', function () {
            ticketId = $(this).attr('data-ticketId');

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0CC27E',
                cancelButtonColor: '#FF586B',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success mr-5',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(
                function () {
                            $.ajax({
                                            method:     "GEt",
                                            {{--url: "{{URL::to('ajax/tickets/delete')}}",--}}
                                            url: "{{URL::to('tickets/movetotrash/')}}/" + ticketId,
                                            data: {ticketId: ticketId },
                                            dataType:"HTML",
                                            success: function(result){
                                                swal(
                                                    'Deleted!',
                                                    'Ticket has been deleted.',
                                                    'success'
                                                )

                                            },
                                            error: function(results){
                                                swal(
                                                    'Error',
                                                    results,
                                                    'error'
                                                )
                                            }
                                });

            }, function (dismiss) {
                if (dismiss === 'cancel') {
                    swal(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    )
                }
            }
            )
        });
    </script>

@endsection
