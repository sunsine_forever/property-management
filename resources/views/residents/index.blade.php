@extends('layouts.master')

@section('page-css')

    <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection


@section('main-content')
    @include('partials.flash-message')

    <div class="breadcrumb">
        <h1>{{$pageTitle}}</h1>
        {{--<ul>--}}
        {{--<li><a href="">Dashboard</a></li>--}}
        {{--<li>List</li>--}}
        {{--</ul>--}}
    </div>
    <div class="separator-breadcrumb border-top"></div>

    <a href="javascript:;" class="btn  btn-outline-dark btn-sm m-1"> Export </a>
    <a href="{{url('building/residents/'.$buildingId.'/add')}}" class="btn btn-success btn-sm m-1"> <i class="i-Add"></i> Resident </a>


    <div class="row">
        <!-- start col-md-12 -->
        <div class="col-md-12">

            <div class="card text-left">

                <div class="card-body">
                    <h4 class="card-title mb-3">{{$pageTitle}}</h4>

                    <div class="table-responsive">

                        <table id="sample_1" class="display table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr class="uppercase">
                                <th> #  </th>
                                <th> Full name   </th>
                                <th> Email  </th>
                                <th> Contact number </th>
                                <th> Unit  </th>
                                <th> Group  </th>
                                <th>  Remote  </th>
                                <th> Parking </th>
                                <th> Locker </th>
                                <th> Access Key </th>
                                <th></th>
                            </tr>

                            </thead>
                        </table>
                    </div>

                </div>
            </div> <!-- end col-md-12 -->
        </div>

    </div>

@endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    {{--<script src="{{asset('assets/js/datatables.script.js')}}"></script>--}}



    <script>

        $(document).ready(function() {

            $('#sample_1').DataTable( {
                "bStateSave": true,

                // save custom filters to the state
                "fnStateSaveParams":    function ( oSettings, sValue ) {
                    $("#datatable_ajax tr.filter .form-control").each(function() {
                        sValue[$(this).attr('name')] = $(this).val();
                    });

                    return sValue;
                },

                // read the custom filters from saved state and populate the filter inputs
                "fnStateLoadParams" : function ( oSettings, oData ) {
                    //Load custom filters
                    $("#datatable_ajax tr.filter .form-control").each(function() {
                        var element = $(this);
                        if (oData[element.attr('name')]) {
                            element.val( oData[element.attr('name')] );
                        }
                    });

                    return true;
                },
                "processing": true,
                "serverSide": true,
                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
                "ajax": {
                    "url": "{{url('ajax/residents')}}?buildingId={{$buildingId}}",
                    "type": "POST"
                },
                "columns": [
                    { "data":'row_num',"orderable":false},
                    { "data": function (data, type, dataToSet) {
                            return  data.title + ' ' + data.first_name + ' ' + data.last_name;
                        }, "orderable": false},
                    { "data": function (data, type, dataToSet) {
                            return 'Email : ' + data.email + '<br> Second email: '+ data .second_email ;
                        }, "orderable": false },
                    { "data": function (data, type, dataToSet) {
                            var contact  =  'Mobile : ' + data.mobile_number + '<br> Home : '+ data.home_number + '<br> Office: ' + data.office_number
                            return contact;
                        }, "orderable": false},
                    {"data": "livingunit_id", "orderable": false},
                    {"data": "group_name", "orderable": false},
                    {"data": "remote_number", "orderable": false},
                    {"data": "parking_number", "orderable": false},
                    {"data": "locker_number", "orderable": false},
                    {"data": "access_key_number", "orderable": false},
                    { "data": "row_id", "orderable":false, "render": function (data) {
                            var string = '<button type="button" class="btn bg-white _r_btn border-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                                '<span class="_dot _inline-dot bg-primary"></span>' +
                                '<span class="_dot _inline-dot bg-primary"></span>' +
                                '<span class="_dot _inline-dot bg-primary"></span>  ' +
                                '</button>  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 33px, 0px); top: 0px; left: 0px; will-change: transform;">'

                            string = string + '<a class="dropdown-item ul-widget__link--font" href="{{ url('building/residents/'.$buildingId.'/edit/') }}/'+ data + '">  <i class="i-Pen-2"> </i> Edit </a>  </div>';
                            return string
                        }
                    }

                ],
                // "order": [0]// set first column as a default sort by asc
                "order": [[ 0, "desc" ]]
            } );
        } );


    </script>

@endsection
