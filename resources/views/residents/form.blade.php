@extends('layouts.master')
@section('before-css')
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection
@section('page-css')

    <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection


@section('main-content')

    @include('partials.errors')
    @include('partials.flash-message')


    <div class="breadcrumb">
        <h1>{{$pageTitle}}</h1>

    </div>

    <div class="separator-breadcrumb border-top"></div>
    <div class="row">

        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="card-title mb-3">{{$pageTitle}} <span style="color: #969696;">- {{$subBtnText}}</span></div>
                    @if(isset($resident))
                        <form method="post" action="{{ url('building/residents/'.$buildingId.'/update/'.$resident->id) }}" class="needs-validation" novalidate>
                            @method('PATCH')

                            @else
                                <form action="{{ url('building/residents/'. $buildingId.'/save') }}" method="POST" class="needs-validation" novalidate>
                                    @endif
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-1 form-group mb-3">
                                            <label for="title">Title</label>
                                            <select name="title" id="title" class="form-control">
                                                @foreach($titles as $title)
                                                    <option value="{{$title}}" @if(isset($resident->$title) && $resident->$title == $title ) selected @endif> {{$title}} </option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-5 form-group mb-3">
                                            <label for="first_name">First name </label>
                                            <input type="text" class="form-control" id="first_name" placeholder="Enter first name"  value="{{{$resident->first_name ?? '' }}}" name="first_name">
                                            <div class="invalid-feedback">
                                                Please provide a brand type.
                                            </div>
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="last_name">Last name </label>
                                            <input type="text" class="form-control" id="last_name" placeholder="Last name" value="{{{$resident->last_name ?? '' }}}" name="last_name" required>
                                            <div class="invalid-feedback">
                                                Please provide a last name.
                                            </div>
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="email">Email</label>
                                            <input class="form-control" id="email" placeholder="Email" name="email" value="{{{$resident->email ?? '' }}}" required>
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="second_email">Second email</label>
                                            <input class="form-control" id="second_email" placeholder="Card" name="second_email" value="{{{$resident->percent_square_footage ?? '' }}}" required>

                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="mobile_number">Mobile number</label>
                                            <input type="text" class="form-control" id="mobile_number" placeholder="Mobile number"  name="mobile_number" value="{{{$resident->mobile_number ?? '' }}}" required>
                                            <div class="invalid-feedback">
                                                Please provide a valid province.
                                            </div>
                                        </div>


                                        <div class="col-md-6 form-group mb-3">
                                            <label for="home_number">Home number</label>
                                            <input type="text" class="form-control" id="home_number" placeholder="Home number" name="home_number" required value="{{{$resident->home_number ?? '' }}}">
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="office_number">Office number </label>
                                            <input type="text" class="form-control" id="office_number" placeholder="Office number" name="office_number" value="{{{$resident->office_number ?? '' }}}">
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="office_number_ext">Office number </label>
                                            <input type="text" class="form-control" id="office_number_ext" placeholder="Office number ext." name="office_number_ext" value="{{{$resident->office_number_ext ?? '' }}}">
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="emergency_contact">Emergency contact</label>
                                            <input type="text" class="form-control" id="emergency_contact" placeholder="Emergency contact" required name="emergency_contact" value="{{{$resident->emergency_contact ?? '' }}}" >
                                            <div class="invalid-feedback">
                                                Please provide a emergency contact.
                                            </div>
                                        </div>



                                        <div class="col-md-2 form-group mb-1">
                                            <label for="lastName1">&nbsp;</label>
                                            <label class="checkbox checkbox-outline-primary">
                                                <input type="checkbox" name="email_notification" value="{{{$building->email_notification ?? '' }}}" @if(isset($building->email_notification)) checked @endif >
                                                <span>Email notification</span>
                                                <span class="checkmark"></span>
                                            </label>

                                        </div>

                                        <div class="col-md-2 form-group mb-1">
                                            <label for="lastName1">&nbsp;</label>
                                            <label class="checkbox checkbox-outline-primary">
                                                <input type="checkbox" name="sms_notification" value="{{{$resident->sms_notification ?? '' }}}" @if(isset($resident->sms_notification)) checked @endif >
                                                <span>SMS notification</span>
                                                <span class="checkmark"></span>
                                            </label>

                                        </div>



                                        <div class="col-md-6 form-group mb-3">
                                            <label for="livingunit_id">Unit</label>
                                            <select name="livingunit_id" id="livingunit_id" class="form-control">
                                                @foreach($units as $unit)
                                                <option value="{{$unit->id}}" @if(isset($resident->sms_notification) && $resident->livingunit_id == $unit_id ) selected @endif > {{$unit->id}} </option>
                                                    @endforeach
                                            </select>
                                        </div>


                                        <div class="col-md-6 form-group mb-3">
                                            <label for="group_id">Group</label>
                                            <select name="group_id" id="group_id" class="form-control">
                                                @foreach($groups as $key=>$group)
                                                    <option value="{{$key}}" @if(isset($resident->group_id) && $resident->group_id == $key ) selected @endif> {{$group}} </option>
                                                @endforeach
                                            </select>
                                        </div>


                                        <div class="col-md-6 form-group mb-3">
                                            <label for="remote_id">Remote</label>
                                            <select name="remote_id" id="remote_id" class="form-control">
                                                @foreach($remotes as $key=>$remote)
                                                    <option value="{{$key}}" @if(isset($resident->remote_id) && $resident->remote_id == $key ) selected @endif> {{$remote}} </option>
                                                @endforeach
                                            </select>
                                        </div>



                                        <div class="col-md-6 form-group mb-3">
                                            <label for="parking">Parking</label>
                                            <select name="parking_id" id="parking_id" class="form-control">
                                                @foreach($parkings as $key=>$parking)
                                                    <option value="{{$key}}" @if(isset($resident->parking_id) && $resident->parking_id == $key ) selected @endif> {{$parking}} </option>
                                                @endforeach
                                            </select>
                                        </div>


                                        <div class="col-md-6 form-group mb-3">
                                            <label for="locker_id">Locker</label>
                                            <select name="locker_id" id="locker_id" class="form-control">
                                                @foreach($lockers as $key=>$locker)
                                                    <option value="{{$key}}" @if(isset($resident->locker_id) && $resident->locker_id == $key ) selected @endif> {{$locker}} </option>
                                                @endforeach
                                            </select>
                                        </div>



                                        <div class="col-md-6 form-group mb-3">
                                            <label for="access_key_id">Access key</label>
                                            <select name="access_key_id" id="access_key_id" class="form-control">
                                                @foreach($access_keys as $key=>$access_key)
                                                    <option value="{{$key}}" @if(isset($resident->access_key_id) && $resident->access_key_id == $key ) selected @endif> {{$access_key}} </option>
                                                @endforeach
                                            </select>
                                        </div>



                                        <div class="col-md-12 form-group mb-3">
                                            <label for="note">Note</label>
                                            <textarea name="note" id="" cols="30" rows="10" class="form-control"> {{{$resident->note ?? '' }}} </textarea>
                                        </div>

                                        <br>
                                        <div class="col-md-12">
                                            <button class="btn btn-primary">{{$subBtnText}}</button>
                                            <a href="{{url('building/residents/' . $buildingId)}}" class="btn btn-light"> Cancel</a>
                                        </div>

                                    </div>
                                </form>
                </div>
            </div>
        </div>
    </div>



@endsection

@section('page-js')

@endsection
@section('bottom-js')
    <script src="{{asset('assets/js/form.basic.script.js')}}"></script>
    <script src="{{asset('assets/js/form.validation.script.js')}}"></script>


@endsection